Flight-like HKE Backplane Backend via Redis
===========================================

To use the flight-like backend to redis, we require a few different pieces,
most of which require separate threads. In the future, these pieces will be
constructed automatically by a startup script. For now, absent information
about the working of the front-ends, they must be started separately.

These pieces conceptually match the pieces in the flight software architecture
block diagram, though not always the names. These pieces are:

1) Backplane (physical or emulated)
2) Redis server
3) lib/command_queue_cb.py:CommandQueue (aka port_owner)
4) lib/bp_interpreter.py:BPInterpreter (aka bp_interpreter)

bpconfig file
-----------------------

Construct a .bpconfig file to match the Backplane. Construct derived data
products as desired.

backplane emulation
-------------------

If the backplane needs to be emulated, it may be done using
lib/hke_emulator.py. This step may be skipped if a physical backplane is
available.

Create virtual ports. This works for *nix. Windows users will require cygwin
or some other virtual serial port software. ::

    $ socat -d -d PTY,raw,echo=0,link=/path/to/home/pty1,crnl \
        PTY,raw,echo=0,link=/path/to/home/pty2,crnl

Construct and run the backplane emulator. The emulator creates a backplane to
simulate what is present in a bpconfig file. In a new terminal::

    $ cd /path/to/hkeredis/lib
    $ python
    > from hke_emulator import Emulator
    > em = Emulator(bpconfig='/path/to/boards.bpconfig',
                    port=/path/to/home/pty2)
    > em.start()

redis server
-------------------

Start up the redis server. E.g., in a new terminal, ::

    $ redis-server

It may be useful for diagnostic purposes to also start up a redis monitor,
also in a new terminal::

    $ redis-cli
    > monitor

port_owner
----------

Initialize the port_owner, in a new terminal::

    $ cd /path/to/hkeredis/lib
    $ python command_queue_cb.py

command_queue_cb.py may need to be configured. See the test() function in the
source code to configure.

bp_interpreter
--------------

Initialize the bp_interpreter, in a new terminal::

    $ cd /path/to/hkeredis/lib
    $ python
    > from bp_interpreter import BPInterpreter
    > bpi = BPInterpreter(bpconfig='/path/to/boards.bpconfig')
    > bpi.start()

reading the parsed data
-----------------------

By this point, the data is being pulled off the serial port by port_owner and
pushed into redis, then pulled out of redis by bp_interpreter, parsed, and
pushed by into the parsed data channel. The parsed data products may be pulled
out of redis now.

An example of this process may be found in parse_reader.py. ::

    > import parsed_reader
    > pr = parsed_reader.ToyParsedReader()
    > data = pr.read_data()

The data may be inspected in the structured array, data.