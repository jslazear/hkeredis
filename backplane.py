#!/bin/env python

"""
backplane.py
jlazear
2014-01-23

HKE Backplane controller via redis.

Example:

<example code here>
"""
version = 20140123
releasestatus = 'beta'

import time
import uuid
import serial
from types import StringTypes
import redis
import lib.comm_buffer as cb

class CommandQueue(object):
    """
    HKE command queue using a redis interface.
    """
    def __init__(self, red='127.0.0.1', port=6379, password=None,
                 serport='/dev/tty.usbserial-DAWYA0PI', baudrate=115200,
                 timeout=0., pollperiod=.25,
                 bpname=None, prefix='hke', queue='command_requests',
                 responses='activity_log', errors='errors',
                 username=None):
        if isinstance(red, (redis.Redis, redis.StrictRedis)):
            self.redis_conn = red
            self.pool = None
        elif isinstance(red, redis.ConnectionPool):
            self.pool = red
            self.redis_conn = redis.Redis(connection_pool=self.pool)
        elif isinstance(red, StringTypes):
            self.pool = redis.ConnectionPool(host=red, port=port,
                                             password=password)
            self.redis_conn = redis.Redis(connection_pool=self.pool)

        # self.ser = serial.Serial(port=serport, baudrate=baudrate,
        #                             timeout=timeout)
        self.ser = {'port': serport, 'baudrate': baudrate, 'timeout': timeout} #DELME

        self.userid = str(uuid.uuid1())[9:]
        if bpname is None:
            bpname = self.userid

        if username is None:
            username = bpname
        self.username = username

        self.prefix = prefix
        self.bpname = bpname
        self.keydict = {'prefix': self.prefix,
                        'bpname': bpname}
        baselist = [str(i) for i in (self.prefix, self.bpname) if i]
        basestr = '.'.join(baselist + ['{0}'])
        globallist = [str(i) for i in (self.prefix,) if i]
        globalstr = '.'.join(globallist + ['{0}'])

        self.queue_key = basestr.format(queue)
        self.queue = cb.CommBufferReader(self.redis_conn,
                                         commkey=self.queue_key,
                                         callback='commobj')

        self.global_queue_key = basestr.format(queue)
        self.global_queue = cb.CommBufferReader(self.redis_conn,
                                                commkey=self.global_queue_key,
                                                callback='commobj',
                                                prefix=prefix,
                                                bpname=self.bpname,
                                                username=self.username)

        self.responses_key = basestr.format(responses)
        self.responses = cb.CommBufferWriter(self.redis_conn,
                                             commkey=self.responses_key,
                                             username=self.username)

        self.errors_key = basestr.format(errors)
        self.errors = cb.CommBufferWriter(self.redis_conn,
                                             commkey=self.errors_key,
                                             username=self.username)

        self.cmd_buffer = []
        self.buffer = ''

        self.cont = True
        self.period = pollperiod
        # self._cmddict = {'p': self.p,  # Print to host terminal
        #                  's': self.s}  # Send to serial port
        self._cmddict = {} #DELME

    def start(self):
        while self.cont:
            try:
                self._update_command_queue()
                self._process_cmd()
                self._read_serial_port()
                time.sleep(self.period)
            except KeyboardInterrupt:
                self.cont = False
        else:
            try:
                self.ser.close()
            except AttributeError:
                pass

    def _update_command_queue(self):
        newglobals = self.global_queue.update()
        newcmds = self.queue.update()
        if newglobals or newcmds:
            print "newglobals = ", newglobals #DELME
            print "newcmds = ", newcmds #DELME

        # globals get priority
        self.cmd_buffer = newglobals + self.cmd_buffer
        self.cmd_buffer.extend(newcmds)

    def _process_cmd(self, cmdobj=None):
        if cmdobj is None:
            try:
                cmdobj = self.cmd_buffer.pop(0)
            except IndexError:
                return

        try:
            payload = cmdobj['packet_payload']
            cmd = payload['command']
            args = payload['arguments']
            cmdstr = ' '.join([cmd] + args)

            print "TO SERIAL: {0}".format(cmdstr)
            #self.ser.write(cmdstr)

            logobj = cb.LogObj(cmdobj, logargs=[True],
                               sourcename=self.username)
        except Exception as e:
            print "FAILED TO WRITE, {e}".format(e=e)
            logobj = cb.LogObj(cmdobj, logargs=[False],
                               sourcename=self.username)

        self.responses.send(logobj)

    def _read_serial_port(self):
        return #DELME
        while self.ser.inWaiting():
            readblock = self.buffer + self.ser.read(self.ser.inWaiting())
            readlist = readblock.split('\r\n')
            self.buffer = readlist.pop()
            resplist = [rl for rl in readlist
                        if (not rl.startswith('*') and rl != '')]

            if resplist and (resplist != ['']):
                print "Sending to hke.response {0}".format(str(resplist)) #DELME
                # self.redis.rpush(self._responsename, *resplist)
                # self.redis.ltrim(self._responsename, -self.maxresponses, -1)
                self._push_and_trim(*resplist)

    def _push_and_trim(self, *values):
        print "values = ", values #DELME
        return
        pipe = self.redis.pipeline()
        num = len(values)
        pipe.rpush(self._responsename, *values)
        pipe.ltrim(self._responsename, -self.maxresponses, -1)
        pipe.incr(self._responseindex, num)
        pipe.execute()