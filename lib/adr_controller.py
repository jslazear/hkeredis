#!/bin/env python

"""
adr_controller.py
jlazear
2014-09-25

Interfaces with redis to monitor the ADR state and actuate the ADR.

Long description

Example:

<example code here>
"""
version = 20140925
releasestatus = 'beta'

import time

import redis
import numpy as np

import comm_buffer as cb
from parse_adr_cycle import ADRFileParser


class ADRController(object):
    """
    State machine for controlling the ADR.

    The state machine is generated by parsing a ADR cycle file, which then
    constructs a series of state with conditional transitions. Since the input
    to the conditions is not known a priori, this class generates a series of
    check functions.
    """
    def __init__(self, bpconfig='boards.bpconfig',
                 host='127.0.0.1', port=6379, password=None,
                 prefix='hke', bpname='testbp',  # backplane prefices
                 adrname='adr',                  # adr command queue name
                 queuename='command_requests',   # backplane command queue name
                 responsename='activity_log',    # to send messages to user
                 parsedname='parsed_data',       # collect current parsed data
                 pollperiod=.5, maxlen=10000, maxprocess=100,
                 cmd_period=1.,
                 mode='redis',                   # communication mode
                 **kwargs):
        self.period = pollperiod

        self.mode = mode
        if mode == 'redis':
            self.redis = redis.Redis(host=host, port=port, password=password)


            kwargs = {'red': host, 'port': port, 'password': password,
                      'prefix': 'hke', 'bpname': bpname,
                      'maxlen': maxlen, 'max_process': maxprocess,
                      'username': 'bp_interpreter'}

            # Commands to be handled by adr_controller come in to listen_buffer
            self.listen_buffer = cb.CommBufferReader(commkey=adrname,
                                                     callback='commobj',
                                                     **kwargs)

            # Backplane data parsed by bp_interpreter is dumped here
            self.parsed_data = cb.CommBufferReader(commkey=parsedname,
                                                   **kwargs)

            # Messages sent by the adr_controller to user go into here
            self.response_buffer = cb.CommBufferWriter(commkey=responsename,
                                                       **kwargs)

            # Commands to be issued by adr_controller to backplane go here
            self.backplane_buffer = cb.CommBufferWriter(commkey=queuename,
                                                        **kwargs)

            self.parsed_data_key = self.parsed_data.key
            self.dtype_key = self.parsed_data_key + '_header'
            self.dt = self._get_data_dtype()

            self.cont = True
        elif mode == 'serial':  #DELME? Want to keep serial implementation?
            from serial import Serial
            try:
                self.listen_buffer = Serial(kwargs['listenport'])
                self.buf = ''
                self.parsed_data = Serial(kwargs['parsedport'])
                self.response_buffer = Serial(kwargs['responseport'])
                self.backplane_buffer = Serial(kwargs['backplaneport'])
                self.dt = kwargs['dt']
            except KeyError:
                raise ValueError('In serial mode, must specify: listenport, '
                                 'parsedport, responseport, backplaneport, dt.')

            self.cont = True
        else:
            self.listen_buffer = None
            self.parsed_data = None
            self.response_buffer = None
            self.backplane_buffer = None

            self.cont = False

        self.file = None
        self.current_state = None
        self._current_states_tree = None
        self.states = []
        self._state_dict = {}
        self._current_data = {}

        self._parser = ADRFileParser(bpconfig=bpconfig)
        self.cmd_period = cmd_period

        self._cmddict = {'load': self._load_cycle,
                         'state': self._set_state,
                         'quit': self._quit}

    def __del__(self):
        if self.mode == 'serial':
            self.listen_buffer.close()
            self.parsed_data.close()
            self.response_buffer.close()
            self.backplane_buffer.close()

    def read_cycle_file(self, fname):
        """
        Reads a cycle file and populate the state machine.
        """
        states_tree = self._parser.parse_from_file(fname)
        self._current_states_tree = states_tree
        State._state_index = 0  # Reset state counter

        for state_node in states_tree:
            state = State(state_node, cmd_period=self.cmd_period, parent=self)
            name = state.name
            self.states.append(state)
            self._state_dict[name] = state
        self.transition(destination=0)
        # self.current_state = self.states[0]
        self.file = fname

    def update(self, data=None):
        """
        Checks for and performs the first valid transition.

        Uses the dictionary `data` as an input to the expressions. If `data`
        is None, uses the most up-to-date data the adr_controller has.
        """
        if data is None:
            data = self._current_data
        (transition, destination) = self.current_state.update(data)
        if transition:
            print "Found valid transition!" #DELME
            self.transition(destination, data)

    def transition(self, destination=None, data=None):
        """
        Transitions to the selected state. If destination is None, transitions
        to the next state. If destination is None and in the last state, does
        nothing.
        """
        if data is None:
            data = self._current_data
        if destination is None:
            i = self.current_state.index
            next_state = self.states[i+1]
        else:
            next_state = self._lookup_destination(destination)

        transstr = "TRANSITION: {0} ---> {1}\r\n"  #DELME
        ni = self.current_state.name if self.current_state else '<None>'
        nf = next_state.name
        self.response_buffer.write(transstr.format(ni, nf))  #DELME
        self.current_state = next_state
        self._t_state_start = time.time()
        next_state.execute(data)

    def _lookup_destination(self, destination):
        if destination in self._state_dict.keys():
            return self._state_dict[destination]
        else:
            try:
                intdestination = int(destination)
                return self.states[intdestination]
            except (ValueError, IndexError):
                errmsg = 'Invalid destination state: {0}'
                raise ADRControllerError(errmsg.format(destination))

    def _get_data_dtype(self):
        """Get the current data dtype from redis."""
        dtstr = self.redis.get(self.dtype_key)
        self.dt = np.dtype(eval(dtstr))

    def read_data(self):
        """
        Read some data. The amount depends on how the data_reader was
        configured. Always reads the NEXT N packets, not the last N.

        Returns a Numpy record array of all data packets received.

        Updates self._current_data with a DICTIONARY with the names and
        values of only the latest data packet.
        """
        datastr = ''.join(self.parsed_data.update())
        data = np.frombuffer(datastr, dtype=self.dt)
        if data:
            names = data.dtype.names
            self._current_data = dict(zip(names, data[-1]))
        return data

    def _process_commands(self):
        if not self.listen_buffer:
            return
        elif self.mode == 'redis':
            cmds = self.listen_buffer.update()

            if cmds: print cmds  #DELME

            for cmd in cmds:
                print "Received cmd:", cmd()  #DELME
                payload = cmd['packet_payload']
                command = payload['command']
                args = payload['arguments']
                try:
                    self._cmddict[command](*args)
                    logcmd = cb.LogObj(cmd, logargs=['success'])
                    self.response_buffer.send(logcmd)
                except KeyError:
                    self._default(cmd, *args)
                    logcmd = cb.LogObj(cmd, logargs=['failed', 'invalid command'])
                    self.response_buffer.send(logcmd)
                except TypeError:
                    logcmd = cb.LogObj(cmd, logargs=['failed', 'invalid argument'])
                    self.response_buffer.send(logcmd)
        elif self.mode == 'serial':
            self.buf += self.listen_buffer.read(self.listen_buffer.inWaiting())
            # print "buf = ", repr(self.buf) #DELME
            if '\n' in self.buf:
                cmds = self.buf.split('\n')
                self.buf = cmds.pop()
            else:
                cmds = []

            # print "cmds = ", cmds #DELME
            for cmd in cmds:
                cmd = cmd.strip()
                try:
                    first, args = cmd.split(' ', 1)
                    first = first.strip()
                    args = args.split()
                except ValueError:
                    first = cmd
                    args = []
                print "Received cmd:", repr(cmd)
                try:
                    self._cmddict[first](*args)
                    self.response_buffer.write('ISSUED: {0}\r\n'.format(cmd))
                except KeyError:
                    self._default(*args)
                    errmsg = 'INVALID COMMAND: {0}\r\n'
                    self.response_buffer.write(errmsg.format(cmd))
                except TypeError:
                    errmsg = 'INVALID ARGUMENTS: {0}\r\n'
                    self.response_buffer.write(errmsg.format(cmd))

    def _default(self, *args):
        pass

    def start(self):
        while self.cont:
            try:
                self._process_commands()
                # self.read_data()  #DELME #FIXME
                self.update()
                time.sleep(self.period)
            except KeyboardInterrupt:
                print "Received KeyboardInterrupt. Unset continue flag."
                self.cont = False
        else:  #DELME? Want to keep serial implementation?
            if self.mode == 'serial':
                self.listen_buffer.close()
                self.parsed_data.close()
                self.response_buffer.close()
                self.backplane_buffer.close()


    def quit(self):
        self.cont = False

    def _load_cycle(self, fname):
        """Load a cycle file."""
        self.read_cycle_file(fname)

    def _set_state(self, state):
        """Transition to the specified state."""
        try:
            self.transition(state)
        except ADRControllerError as e:
            msg = str(e)

    def _quit(self, *args):
        """Stop the loop."""
        self.cont = False

    def respond(self, msg):
        """Respond to the user with a message."""
        if self.response_buffer is None:
            print msg
        elif self.mode == 'serial':
            self.response_buffer.write(msg)
        elif self.mode == 'redis':
            msgobj = cb.DataObj(msg, 'message')
            self.response_buffer.send(msgobj)

    def command_backplane(self, cmd):
        """Issue a command to the backplane."""
        if self.backplane_buffer is None:
            print cmd
        elif self.mode == 'serial':
            self.backplane_buffer.write(cmd + '\r\n')
        elif self.mode == 'redis':
            cmdobj = cb.CommandObj(cmd)
            self.backplane_buffer.send(cmdobj)


class State(object):
    """
    An individual ADR state.

    The input is the node subtree of a full state.
    """
    _state_index = 0

    def __init__(self, state_node, cmd_period=1., parent=None):
        self.parent = parent
        self._statedict = {'name': self._name,
                           'cmd': self._cmd,
                           'waitfor': self._waitfor,
                           'message': self._message}

        self.index = State._state_index + 0
        self.name = str(self._state_index)

        self.message = ''
        self.commands = []
        self.waitfors = []

        self.cmd_period = cmd_period  # Wait period between issuing commands

        self._nodedict = {'float': self._node_float,
                          'identifier': self._node_identifier,
                          'compare': self._node_binary,
                          'or': self._node_binary,
                          'and': self._node_binary,
                          'not': self._node_not,
                          'expression': self._node_expression}


        # Step through the lines of the tree and take the appropriate actions
        # for each line, depending on its node type.
        for line_node in state_node:
            linetype = line_node.nodetype
            self._statedict[linetype](line_node)

        # If no WAITFOR conditions specified, proceed to next state
        if not self.waitfors:
            self.waitfors.append((self._true, None))

        State._state_index += 1

    def update(self, data):
        """
        Updates the conditions and returns the first valid transition, if any.
        """
        for condition, destination in self.waitfors:
            if condition(data):
                return (True, destination)
        return (False, None)

    def execute(self, data):
        """Executes commands in the state."""
        # Can't use a straight cmd.format(**data) call, since the keys in
        # data may have special characters, e.g. ':', which format does not
        # permit. Collect matches into a list and generate .format-able
        # replacement command string, then format that with the list.
        for cmd in self.commands:
            fmtlist = []
            cmdlist = cmd.split()
            j = 0
            for i, identifier in enumerate(cmdlist):
                if identifier in data.keys():
                    cmdlist[i] = '{' + str(j) + '}'
                    datum = data[identifier]
                    try:
                        datum = str(int(float(datum)))
                    except ValueError:
                        datum = str(datum)
                    fmtlist.append(datum)
                    j += 1

            newcmd = ' '.join(cmdlist)
            cmdstr = newcmd.format(*fmtlist)
            self._write(cmdstr)
            time.sleep(self.cmd_period)

    def _name(self, name_node):
        name = name_node.nodevalue
        self.name = name

    def _cmd(self, cmd_node):
        cmd = cmd_node.nodevalue
        self.commands.append(cmd)

    def _message(self, message_node):
        message = message_node.nodevalue
        self.message = message

    def _waitfor(self, waitfor_node):
        self._destination = None
        nfunc = self._node_waitfor(waitfor_node)
        waitfor = (nfunc, self._destination)
        self.waitfors.append(waitfor)

    def _write(self, cmd):
        self.parent.command_backplane(cmd)

    def _node_waitfor(self, node):
        n0 = node[0]
        ntype = n0.nodetype
        nfunc = self._nodedict[ntype](n0)
        return nfunc

    def _node_expression(self, node):
        if node.nodevalue == 'expr_goto':
            c0, c1 = node
            nfunc = self._nodedict[c0.nodetype](c0)
            self._destination = c1.nodevalue
            return nfunc
        elif node.nodevalue == 'expr':
            c0 = node[0]
            if len(node) == 1 and c0.nodetype == 'identifier':
                nfunc = self._node_singleton(c0)
            else:
                nfunc = self._nodedict[c0.nodetype](c0)
            return nfunc
        elif node.nodevalue == 'goto':
            c0 = node[0]
            nfunc = self._true
            self._destination = c0.nodevalue
            return nfunc

    def _node_binary(self, node):
        nfuncs = [self._nodedict[child.nodetype](child) for child in node]
        n0, n1 = nfuncs
        op = node.nodevalue
        if op == '<':
            return lambda data: n0(data) < n1(data)
        elif op == '>':
            return lambda data: n0(data) > n1(data)
        elif op == 'and':
            return lambda data: n0(data) and n1(data)
        elif op == 'or':
            return lambda data: n0(data) or n1(data)
        else:
            errmsg = 'Invalid compare operator: "{0}"'
            raise ADRControllerError(errmsg.format(op))

    def _node_not(self, node):
        n0 = node[0]
        nfunc = self._nodedict[n0.nodetype](n0)
        return lambda data: not nfunc(data)

    def _node_identifier(self, node):
        value = node.nodevalue

        # Time identifiers must be handled differently
        timestrs = ('second', 'seconds', 'minute', 'minutes', 'hour', 'hours')
        if value in timestrs:
            modifiers = {'second': 1., 'seconds': 1.,
                         'minute': 1./60, 'minutes': 1./60,
                         'hour': 1./3600., 'hours': 1./3600}
            modifier = modifiers[value]
            f = lambda data: modifier*(time.time() - self.parent._t_state_start)
        else:
            f = lambda data: data[value]
        return f

    def _node_float(self, node):
        return lambda data: node.nodevalue

    def _node_singleton(self, node):
        singleton = node.nodevalue.lower()
        d = {'forever': self._false}
        return d[singleton]

    @staticmethod
    def _false(*args, **kwargs):
        return False

    @staticmethod
    def _true(*args, **kwargs):
        return True


class ADRControllerError(Exception):
    pass