#!/bin/env python

"""
analogin.py
jlazear
2014-04-16

Virtual class for parsing AnalogIn data.

Modified from BOBCATT-style analogin.py. Contains only the code for parsing
AnalogIn data, not for controlling a AnalogIn board.
"""
version = 20140416
releasestatus = 'beta'


class AnalogIn(object):
    """
    Virtual class for parsing AnalogIn data. Never needs initialization.
    """
    @classmethod
    def handlepacket(cls, packet):
        """
        Handles a single packet.

        :Returns:

            `frame` : int
            frame counter for this packet.

            `data` : dict
            A dictionary containing the raw and interpreted AnalogIn data,
            in the boards_spec_v2 format.
        """
        data = cls.parse_analogin_string(packet)
        brdn = data.keys()[0].split(':', 1)[0]
        framekey = brdn + ':frame:ch0'
        frame = data[framekey]
        return frame, data

    @staticmethod
    def parse_analogin_string(s):
        """
        Parses an output string (just one packet) from the AnalogIn
        and returns the information contained in it.
        """
        if s[0] != '*':
            s = '*' + s

        # Extract raw values
        addr_str = s[1:3]
        address = int(addr_str, 16)  # Characters 1 and 2 are Board address
        cardtype = s[3]   # card type ('I' = AnalogIn)
        frame =  int(s[4:6], 16)
        raw_gain = s[6]      # Gain (0=gain of 1, 1=gain of 10,
                             # 2=gain of 100, T=AD590 temperature
                             # mode)
        status = int(s[7], 16)    # Status

        sam_chan_frame = int(s[8:10], 16) # Samples per channel per
                                          # frame (defaults to 1)
        num_chan = int(s[10:12], 16)      # Num channels (1, 2, 4, 8,
                                          # 16, 32, default to 32)
        data = s[12:]     # Remaining characters are data
        num = len(data)/4 # Number of samples in the packet

        raw_ais = [int(data[i:i+4], 16) for i in range(num)]

        brdn = 'brd{0}'.format(addr_str)

        data = {brdn + ':addr:ch0': address,
                brdn + ':type:ch0': cardtype,
                brdn + ':frame:ch0': frame,
                brdn + ':raw_gain:ch0': raw_gain,
                brdn + ':status:ch0': status,
                brdn + ':nsamples:ch0': sam_chan_frame,
                brdn + ':nchan:ch0': num_chan}

        for i in range(len(raw_ais)):
            raw_ai = raw_ais[i]
            raw_ai_key = brdn + ':raw_ai:ch{0}'.format(i)

            data[raw_ai_key] = raw_ai

        return data
