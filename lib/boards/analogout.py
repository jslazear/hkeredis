#!/bin/env python
"""
analogout.py
jlazear
2014-04-16

Virtual class for parsing AnalogOut data.

Modified from BOBCATT-style analogin.py. Contains only the code for parsing
AnalogOut data, not for controlling a AnalogOut board.
"""
version = 20140416
releasestatus = 'dev'


class AnalogOut(object):
    """
    Virtual class for parsing AnalogIn data. Never needs initialization.
    """
    @classmethod
    def handlepacket(cls, packet):
        """
        Handles a single packet.

        :Returns:

            `frame` : int
            frame counter for this packet.

            `data` : dict
            A dictionary containing the raw and interpreted AnalogOut data,
            in the boards_spec_v2 format.
        """
        data = cls.parse_analogout_string(packet)
        brdn = data.keys()[0].split(':', 1)[0]
        framekey = brdn + ':frame:ch0'
        frame = data[framekey]
        return frame, data

    @staticmethod
    def parse_analogout_string(packet):
        """
        Parses an output string (just one packet) from the AnalogOut
        and returns the information contained in it.
        """
        if s[0] != '*':
            s = '*' + s

        # Extract raw values
        addr_str = packet[1:3]
        address = int(addr_str, 16)  # Characters 1 and 2 are Board address
        cardtype = packet[3]   # card type
        frame =  int(packet[4:6], 16)
        unused = packet[6]
        status = int(packet[7], 16)    # Status

        data = packet[8:]
        num = len(data)/3

        auxs = [int(data[i:i+3], 16) for i in range(num)]

        brdn = 'brd{0}'.format(addr_str)

        data = {brdn + ':addr:ch0': address,
                brdn + ':type:ch0': cardtype,
                brdn + ':frame:ch0': frame,
                brdn + ':unused:ch0': unused,
                brdn + ':status:ch0': status}

        for i in range(len(auxs)):
            aux = auxs[i]
            auxkey = brdn + ':ao:ch{0}'.format(i)

            data[auxkey] = aux

        return data
