#!/bin/env python
"""
backplane.py
jlazear
2014-04-15

Parses backplane data.

Modified from the BOBCATT-style backplane.py. Contains only the code for
parsing the backplane data, not for controlling the backplane.

Example usage:

# OLD!
#boardsdict = {'tread': (1, 2, 3), 'ai':(4, 5, 6), 'pmaster': 255}
#bp = Backplane(boardsdict, port='/home/jlazear/pty2')
#bp.listeners.extend([listener1, listener2])
3bp.initialize()
"""
version = 20140415
releasestatus = 'beta'

import time
import string
import os
import datetime
from collections import OrderedDict

import tread
import analogin
import pmaster
import analogout
import dspid


class Backplane(object):
    """
    Distributes packets to be parsed and collates parsed data.

    :Arguments:
        `boards` - (dict) dictionary of boards present in the
                   backplane

    :Attributes:
        `boards` - dictionary of boards present keyed by their addresses
        `buffer` - latest full data set
        `frame` - current frame count
    """
    #====================================================#
    # system methods
    #----------------------------------------------------#
    def __init__(self, bpconfig,
                 notifyfunc=None):
        self._boarddict = {'tread': tread.TRead,
                           'tread_standard': tread.TRead,
                           'tread_lr': tread.TRead,
                           'tread_hr': tread.TRead,
                           'tread_diode': tread.TRead,
                           'analogin': analogin.AnalogIn,
                           'analogout': analogout.AnalogOut,
                           'pmaster': pmaster.PMaster,
                           'dspid': dspid.DSPID}

        self.boards = self._construct_boards_objects(bpconfig)

        # Initialized data buffers and their locks
        self.frame = {}         # Store frame for each address
        for address in self.boards.keys():
            self.frame[address] = -1
        self.data = OrderedDict()      # Public data dictionary
        self._data = OrderedDict()     # Private data dictionary
        self.packets = []   # Public packets list
        self._packets = []  # Private packets list

        if notifyfunc is None:
            self._notifyfunc = self._pass
        else:
            self._notifyfunc = notifyfunc

    def _construct_boards_objects(self, boards):
        """
        Constructs a dictionary of the board virtual objects.
        """
        board_objs = {}
        for addr, bdict in boards['boards'].items():
            btype = bdict['type']
            bobj = self._boarddict[btype]
            board_objs[addr] = bobj
        return board_objs

    def handlepacket(self, packet):
        """
        Handles a packet received by the backplane.

        Handles the packet in the following way:
            1) Figures out which board the packet came from
            2) Passes the packet to the corresponding board object
            3) The board object parses the packet and returns data
            4) If frame count is changed, copy data to public buffer,
               write to disk, and inform listeners of new data
            5) Stores data locally in object
        """
        # Condition packet to pristine form
        if not packet.startswith('*'):
            packet = '*' + packet
        if not packet.endswith('\n'):
            if not packet.endswith('\r'):
                packet += '\r\n'
            else:
                packet += '\n'

        # Find board that sent packet via its address
        address = int(packet[1:3], 16)
        board = self.boards[address]

        # Pass packet to board for parsing
        frame, data = board.handlepacket(packet)

        # Update public data buffer and frame count if there is a new
        # frame, notify listeners
        oldframe = self.frame[address]
        if (address == 255) and (frame != oldframe):
            self.data = self._data.copy()
            self.packets = self._packets[:]
            if oldframe != -1:
                self.notify()
            self._packets = []

        # Store data to private buffer
        self.frame[address] = frame
        for key, value in data.items():
            self._data[key] = value
        self._packets.append(packet)

    def notify(self):
        """
        Notify listeners of new data.
        """
        self._notifyfunc()

    def getdata(self):
        """
        Returns the latest full data set, i.e. the current public
        data.

        :Arguments:
            None

        :Returns:
            `data` - (dict) A dictionary of the latest dataset, with
                     keys corresponding to the board addresses and
                     values corresponding to the data sets for that
                     board. Also included in the dictionary is a
                     'framecount' key, with corresponding value the
                     integer frame count for that data set.
        """
        return self.data.copy()

    def _pass(self):
        pass


# ============================================================
# Exception Classes
# ============================================================

class BackplaneError(Exception):
    """
    Generic Backplane error.
    """
    pass


class UnknownBoardError(BackplaneError):
    """
    An error indicating that the specified board is not of a known
    type.
    """
    def __init__(self, boardstr):
        self.boardstr = boardstr
        self.msg = "Unknown board type: {0}".format(self.boardstr)

    def __str__(self):
        return self.msg


class InvalidAddressError(BackplaneError):
    """
    An error indicating that an invalid address has been entered.
    """
    def __init__(self, address):
        self.address = address
        self.msg = "Invalid address: {0}".format(self.address)

    def __str__(self):
        return self.msg


class InvalidArgumentsError(BackplaneError):
    """
    An error indicating that invalid arguments were passed to a
    Backplane method.
    """
    def __init__(self, arguments):
        self.arguments = arguments
        self.msg = "Invalid arguments: {0}".format(self.arguments)

    def __str__(self):
        return self.msg


class InvalidMessageError(BackplaneError):
    """
    An error indicating that an invalid message was passed to a
    Backplane object.
    """
    def __init__(self, message):
        self.message = message
        self.msg = "Invalid message: {0}".format(self.message)

    def __str__(self):
        return self.msg


class ResponseError(BackplaneError):
    """
    An error indicating that an invalid address has been entered.
    """
    def __init__(self, address):
        self.address = address
        self.msg = ("Timed out while waiting for a response in address: "
                    "{0}").format(self.address)

    def __str__(self):
        return self.msg


class UnknownAddressError(BackplaneError):
    """
    An error indicating that a packet was received from an address
    that the Backplane instance does not think contains a board.
    """
    def __init__(self, address, packet):
        self.address = address
        self.packet = packet
        self.msg = "Received a packet from an address that the \
Backplane object does not think contains a board:\nAddress: \
{0}\nPacket: {1}".format(address, packet)

    def __str__(self):
        return self.msg
