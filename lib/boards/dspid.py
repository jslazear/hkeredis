#!/bin/env python

"""
dspid.py
jlazear
2014-04-15

Virtual class for parsing DSPID data.

Contains only the code for parsing DSPID data, not for controlling a
DSPID board.
"""
version = 20140415
releasestatus = 'beta'


class DSPID(object):
    """
    Virtual class for parsing DSPID data. Never needs initialization.
    """
    @classmethod
    def handlepacket(cls, packet):
        """
        Handles a single packet.

        :Returns:

            `frame` : int
            frame counter for this packet.

            `data` : dict
            A dictionary containing the raw and interpreted DSPID data, in the
            boards_spec_v2 format.
        """
        data = cls.parse_dspid_string(packet)
        brdn = data.keys()[0].split(':', 1)[0]
        framekey = brdn + ':frame:ch0'
        frame = data[framekey]
        return frame, data

    @classmethod
    def parse_dspid_string(cls, string):
        """
        Parses an output string (just one packet) from the DSPID
        and returns the information contained in it.
        """
        if string[0] != '*':
            string = '*' + string

        # Extract raw values
        addr_str = string[1:3]
        address = int(addr_str, 16)  # Characters 1 and 2 are Board address
        cardtype = string[3]   # card type ('D' = DSPID)
        frame =  int(string[4:6], 16)
        tmux = string[6]      # TMux setting
        status = int(string[7], 16)    # Status

        Ndemod = 8
        raw_demods = []
        raw_coilIs = []
        raw_coilDACs = []
        for i in range(Ndemod):
            start = 8 + 16*i
            demod = int(string[start:start+8], 16)
            if demod > 2**31:
                demod -= 2**32
            coilI = int(string[start+8:start+12], 16)
            coilDAC = int(string[start+12:start+16], 16)

            raw_demods.append(demod)
            raw_coilIs.append(coilI)
            raw_coilDACs.append(coilDAC)

        raw_demod = cls.mean(raw_demods)
        raw_coilI = cls.mean(raw_coilIs)
        raw_coilDAC = cls.mean(raw_coilDACs)

        raw_adac = int(string[136:140], 16)
        raw_gdac = int(string[140:144], 16)
        raw_vmon = int(string[144:148], 16)

        raw_ai = int(string[148:152], 16)
        raw_extT = int(string[152:156], 16)
        raw_brdT = int(string[156:160], 16)
        raw_vsupply = int(string[160:164], 16) + 32768 #DELME #FIXME
        raw_gnd = int(string[164:168], 16) + 32768 #DELME #FIXME

        raw_aouts = []
        Naouts = 4
        for i in range(Naouts):
            raw_aout = int(string[168+4*i:172+4*i], 16)
            raw_aouts.append(raw_aout)

        raw_pid_s = int(string[184:192], 16)
        pid_e = int(string[192:200], 16)
        pid_a = int(string[200:208], 16)
        pid_p = int(string[208:210], 16)
        pid_i = int(string[210:212], 16)

        N_sum = 198 # int(string[212:214], 16)  #DELME #FIXME once DSPID reprogrammed

        # Compute physically meaningful values
        gdac = cls.GDAC_to_gain(raw_gdac)
        adac = cls.ADAC_to_nA(raw_adac)  # nA
        vmon = cls.Vin_to_Vmon(cls.ADC_to_Vin(raw_vmon))

        demodfunc = lambda x: cls.Vin_to_Vpreamp(
                                cls.ADC_to_Vin(
                                cls.demod_to_ADC(x, N_sum)),
                                                  gdac)/(adac*1.e-9)
        coilIfunc = lambda x: cls.Vin_to_Icoil(cls.ADC_to_Vin(x))
        coilDACfunc = lambda x: cls.CDAC_to_Vcoil(x)

        demods = [demodfunc(raw_demod) for raw_demod in raw_demods]
        demod = cls.mean(demods)
        coilIs = [coilIfunc(raw_coilI) for raw_coilI in raw_coilIs]
        coilI = cls.mean(coilIs)
        coilDACs = [coilDACfunc(raw_coilDAC) for raw_coilDAC in raw_coilDACs]
        coilDAC = cls.mean(coilDACs)

        ai = cls.Vin_to_AI0(cls.ADC_to_Vin(raw_ai))

        extT = cls.Vin_to_AD590Temp(cls.ADC_to_Vin(raw_extT))
        brdT = cls.Vin_to_AD590Temp(cls.ADC_to_Vin(raw_brdT))
        vsupply = cls.Vin_to_Vsupply(cls.ADC_to_Vin(raw_vsupply))
        gnd = cls.Vin_to_GND(cls.ADC_to_Vin(raw_gnd))

        aouts = [cls.AOdac_to_Vout(raw_aout) for raw_aout in raw_aouts]

        pid_s = demodfunc(raw_pid_s)

        brdn = 'brd{0}'.format(addr_str)

        data = {brdn + ':addr:ch0': address,
                brdn + ':type:ch0': cardtype,
                brdn + ':frame:ch0': frame,
                brdn + ':tmux:ch0': tmux,
                brdn + ':status:ch0': status,
                brdn + ':raw_demod:ch0': raw_demod,
                brdn + ':demod:ch0': demod,
                brdn + ':raw_coilI:ch0': raw_coilI,
                brdn + ':coilI:ch0': coilI,
                brdn + ':raw_cdac:ch0': raw_coilDAC,
                brdn + ':cdac:ch0': coilDAC,
                brdn + ':raw_adac:ch0': raw_adac,
                brdn + ':adac:ch0': adac,
                brdn + ':raw_gdac:ch0': raw_gdac,
                brdn + ':gdac:ch0': gdac,
                brdn + ':raw_vmon:ch0': raw_vmon,
                brdn + ':vmon:ch0': vmon,
                brdn + ':raw_ai:ch0': raw_ai,
                brdn + ':ai:ch0': ai,
                brdn + ':raw_extT:ch0': raw_extT,
                brdn + ':extT:ch0': extT,
                brdn + ':raw_brdT:ch0': raw_brdT,
                brdn + ':brdT:ch0': brdT,
                brdn + ':raw_vsupply:ch0': raw_vsupply,
                brdn + ':vsupply:ch0': vsupply,
                brdn + ':raw_gnd:ch0': raw_gnd,
                brdn + ':gnd:ch0': gnd,
                brdn + ':raw_pid_s:ch0': raw_pid_s,
                brdn + ':pid_s:ch0': pid_s,
                brdn + ':pid_e:ch0': pid_e,
                brdn + ':pid_a:ch0': pid_a,
                brdn + ':pid_p:ch0': pid_p,
                brdn + ':pid_i:ch0': pid_i,
                brdn + ':nsum:ch0': N_sum}

        for i in range(len(raw_demods)):
            rd = raw_demods[i]
            rdkey = brdn + ':raw_demod:ch0_{0}'.format(i)
            data[rdkey] = rd

            d = demods[i]
            dkey = brdn + ':demod:ch0_{0}'.format(i)
            data[dkey] = d

            rcd = raw_coilDACs[i]
            rcdkey = brdn + ':raw_cdac:ch0_{0}'.format(i)
            data[rcdkey] = rcd

            cd = coilDACs[i]
            cdkey = brdn + ':cdac:ch0_{0}'.format(i)
            data[cdkey] = cd

            rcur = raw_coilIs[i]
            rcurkey = brdn + ':raw_coilI:ch0_{0}'.format(i)
            data[rcurkey] = rcur

            cur = coilIs[i]
            curkey = brdn + ':coilI:ch0_{0}'.format(i)
            data[curkey] = cur


        for i in range(len(raw_aouts)):
            rao = raw_aouts[i]
            raokey = brdn + ':raw_ao:ch{0}'.format(i)
            data[raokey] = rao

            ao = aouts[i]
            aokey = brdn + ':ao:ch{0}'.format(i)
            data[aokey] = ao

        return data

    @staticmethod
    def demod_to_ADC(value, N_sum):
        """
        Converts from accumulated demod counts to average ADC counts.
        """
        # demod_counts = demod_avg*N_sum
        # require the +32768 since the demod subtraction cancels it out,
        # but ADC_to_Vin assumes it is there.
        return value/float(N_sum) + 32768


    @staticmethod
    def ADC_to_Vin(value):
        """
        Converts from ADC units to Volts before the level-shifter/last
        amplifier.

        Most values have other amplification steps before this step.

        V_in = 4.99*(4.096 V)*(ADC - 32768)/65536
        """
        return 4.99*4.096*(value - 32768)/65536.

    @staticmethod
    def Vin_to_Vpreamp(value, gain):
        """
        Converts from Volts before the level-shifter/last amplifier to
        Volts coming into the pre-amp.
        """
        return value/(gain*100.)

    @staticmethod
    def Vin_to_Vmon(value):
        """
        Converts from Volts before the level-shifter/last amplifier to
        V_mon Volts.
        """
        return value/5.

    @staticmethod
    def Vin_to_Icoil(value):
        """
        Converts from Volts before the level-shifter/last amplifier to
        coil current in Amps.
        """
        # R_sense = .01 Ohms, G = 100, so V/I = (1 V/A).
        return 1.*value

    @staticmethod
    def Vin_to_AI0(value):
        """
        Converts from Volts before the level-shifter/last amplifier to
        voltage at the AI0 input.
        """
        # G = 1 amplifier
        return 1.*value

    @staticmethod
    def Vin_to_AD590Temp(value):
        """
        Converts from Volts before the level-shifter/last amplifier to
        temperature in Kelvin of the AD590 thermometers (EXT_TEMP and
        BOARD_TEMP).
        """
        # V_in = (49.9 mV)*T - 10.24 V
        # T = (V_in + 10.24 V)/(.0499 V/K)
        return (value + 10.24)/.0499

    @staticmethod
    def Vin_to_Vsupply(value):
        """
        Converts from Volts before the level-shifter/last amplifier to
        the positive voltage supply rail in Volts.
        """
        # V_in = Vplus/4.   -- 1/4 voltage divider
        return value/4.

    @staticmethod
    def Vin_to_GND(value):
        """
        Converts from Volts before the level-shifter/last amplifier to
        the GND voltage supply rail.
        """
        # direct input
        return 1.*value

    @staticmethod
    def AOdac_to_Vout(value):
        """
        Converts from DAC units on an AO channel to Volts on the AO channel.
        """
        # V_AO = (.005 V)*D_AO - (10.24 V)
        return .005*value - 10.24

    @staticmethod
    def ADAC_to_nA(value):
        """
        Converts from raw ADAC units to nano Amperes (nA).
        """
        # V_bias/2 = (V_ref/72.5)*ADAC/65535
        # R_bias/2 = 100k + 499
        # I_bias = V_bias/R_bias = V_ref/(72.5*65535*100499. Ohms)*ADAC
        # I_bias ~= (0.0086 nA/count)*ADAC
        # Small disparity between this and dspid.h?
        return value*4.096/(72.5*65535*100499.)*1.e9

    @staticmethod
    def GDAC_to_gain(value):
        """
        Converts from GDAC units to Gain.
        """
        return (2.**16)/value

    @staticmethod
    def CDAC_to_Vcoil(value):
        """
        Converts from raw CDAC counts to output coil voltage in V.
        """
        # V_coil = V_ref*(CDAC/65536 - 0.5)
        return 4.096*(value/65536. - .5)

    @staticmethod
    def mean(a):
        return float(sum(a))/float(len(a))
