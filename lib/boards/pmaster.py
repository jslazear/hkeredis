#!/bin/env python
"""
pmaster.py
jlazear
2014-04-15

Virtual class for parsing TRead data.

Modified from BOBCATT-style tread.py. Contains only the code for parsing
TRead data, not for controlling a TRead board.
"""
version = 20140415
releasestatus = 'beta'


class PMaster(object):
    """
    Virtual class for parsing TRead data. Never needs initialization.
    """
    @classmethod
    def handlepacket(cls, packet):
        """
        Handles a single packet.

        :Returns:

            `frame` : int
            frame counter for this packet.

            `data` : dict
            A dictionary containing the raw and interpreted PMaster data, in
            the boards_spec_v2 format.
        """
        data = cls.parse_pmaster_string(packet)
        brdn = data.keys()[0].split(':', 1)[0]
        framekey = brdn + ':frame:ch0'
        frame = data[framekey]
        return frame, data

    @staticmethod
    def parse_pmaster_string(s):
        """
        Parses an output string (just one packet) from the PMaster
        and returns the information contained in it.
        """
        if s[0] != '*':
            s = '*' + s

        # Extract raw values
        addr_str = s[1:3]
        address = int(addr_str, 16) # Characters 1 and 2 are Board address
        cardtype = s[3]   # card type ('M' = PMaster)
        frame =  int(s[4:6], 16)
        unused = s[6]     # Unused
        status = int(s[7], 16)     # hex value of mask:
                                   # bit    - Description/Settings
                                   # 0 (LSB)- COM Mode (0 - RS232, 1 - Fiber)
                                   # 1      - UBC Frame Count present (1 = yes)
                                   # 2      - CLK Source (0 = Ext, 1 = Int)
                                   # 3 (MSB)- Unused
        ubc = int(s[8:16], 16)     # UBC Frame Count
        pic = int(s[16:24], 16)    # PIC Frame Count

        brdn = 'brd{0}'.format(addr_str)

        data = {
                brdn + ':addr:ch0': address,
                brdn + ':type:ch0': cardtype,
                brdn + ':frame:ch0': frame,
                brdn + ':unused:ch0': unused,
                brdn + ':status:ch0': status,
                brdn + ':ubc:ch0': ubc,
                brdn + ':pic:ch0': pic}

        return data
