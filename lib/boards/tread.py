#!/bin/env python

"""
tread.py
jlazear
2014-04-15

Virtual class for parsing TRead data.

Modified from BOBCATT-style tread.py. Contains only the code for parsing
TRead data, not for controlling a TRead board.
"""
version = 20140415
releasestatus = 'beta'


class TRead(object):
    """
    Virtual class for parsing TRead data. Never needs initialization.
    """
    @classmethod
    def handlepacket(cls, packet):
        """
        Handles a single packet.

        :Returns:

            `frame` : int
            frame counter for this packet.

            `data` : dict
            A dictionary containing the raw and interpreted DSPID data, in the
            boards_spec_v2 format.
        """
        data = cls.parse_tread_string(packet)
        brdn = data.keys()[0].split(':', 1)[0]
        framekey = brdn + ':frame:ch0'
        frame = data[framekey]
        return frame, data

    @classmethod
    def parse_tread_string(cls, string):
        """
        Parses an output string (just one packet) from the T-Read
        and returns the information contained in it.
        """
        if string[0] != '*':
            string = '*' + string

        # Extract raw values
        addr_str = string[1:3]
        address = int(addr_str, 16)  # Characters 1 and 2 are Board address
        cardtype = string[3]   # card type ('T' = TRead)
        frame =  int(string[4:6], 16)
        tmux = string[6]      # TMux setting
        status = int(string[7], 16)    # Status
        modedict = {0: 'standard',
                    1: 'high_res',
                    2: 'low_res',
                    3: 'diode'}
        mode = modedict[status]

        N_sum = int(string[264:266], 16)

        demodfunc = lambda x, gdac, adac: cls.Vin_to_Vpreamp(
                                cls.ADC_to_Vin(
                                cls.demod_to_ADC(x, N_sum, mode)),
                                                  gdac, mode)/(adac*1.e-9)

        Ndemod = 16
        raw_demods = []
        raw_adacs = []
        raw_gdacs = []
        demods = []
        adacs = []
        gdacs = []
        for i in range(Ndemod):
            start = 8 + 16*i
            raw_demod = int(string[start:start+8], 16)
            if raw_demod > 2**31:
                raw_demod -= 2**32
            raw_adac = int(string[start+8:start+12], 16)
            raw_gdac = int(string[start+12:start+16], 16)

            raw_demods.append(raw_demod)
            raw_adacs.append(raw_adac)
            raw_gdacs.append(raw_gdac)

            adac = cls.ADAC_to_nA(raw_adac, mode)
            gdac = cls.GDAC_to_gain(raw_gdac)
            demod = demodfunc(raw_demod, gdac, adac)

            demods.append(demod)
            adacs.append(adac)
            gdacs.append(gdac)

        brdn = 'brd{0}'.format(addr_str)

        data = {brdn + ':addr:ch0': address,
                brdn + ':type:ch0': cardtype,
                brdn + ':frame:ch0': frame,
                brdn + ':tmux:ch0': tmux,
                brdn + ':status:ch0': status,
                brdn + ':mode:ch0': mode,
                brdn + ':nsum:ch0': N_sum}

        for i in range(Ndemod):
            raw_demod = raw_demods[i]
            demod = demods[i]
            raw_adac = raw_adacs[i]
            adac = adacs[i]
            raw_gdac = raw_gdacs[i]
            gdac = gdacs[i]

            raw_demod_key = brdn + ':raw_demod:ch{0}'.format(i)
            demod_key = brdn + ':demod:ch{0}'.format(i)
            raw_adac_key = brdn + ':raw_adac:ch{0}'.format(i)
            adac_key = brdn + ':adac:ch{0}'.format(i)
            raw_gdac_key = brdn + ':raw_gdac:ch{0}'.format(i)
            gdac_key = brdn + ':gdac:ch{0}'.format(i)

            data[raw_demod_key] = raw_demod
            data[demod_key] = demod
            data[raw_adac_key] = raw_adac
            data[adac_key] = adac
            data[raw_gdac_key] = raw_gdac
            data[gdac_key] = gdac

        return data

    @staticmethod
    def demod_to_ADC(value, N_sum, mode):
        """
        Converts from accumulated demod counts to average ADC counts.
        """
        # demod_counts = demod_avg*N_sum
        # require the +32768 since the demod subtraction cancels it out,
        # but ADC_to_Vin assumes it is there.
        denom = N_sum*1.
        if mode == 'diode': denom = 2*denom
        return value/denom + 32768


    @staticmethod
    def ADC_to_Vin(value):
        """
        Converts from ADC units to Volts before the level-shifter/last
        amplifier.

        Most values have other amplification steps before this step.

        V_in = 4.99*(4.096 V)*(ADC - 32768)/65536
        """
        # diode mode is unipolar, so subtraction correctly offsets error
        # in demod_to_ADC
        return 4.99*4.096*(value - 32768)/65536.

    @staticmethod
    def Vin_to_Vpreamp(value, gain, mode):
        """
        Converts from Volts before the level-shifter/last amplifier to
        Volts coming into the pre-amp.
        """
        preamp = 1. if (mode == 'diode') else 100.
        return value/(gain*preamp)

    @staticmethod
    def ADAC_to_nA(value, mode):
        """
        Converts from raw ADAC units to nano Amperes (nA).
        """
        # V_bias/2 = (V_ref/72.5)*ADAC/65535
        # R_bias/2 = 100k + 499
        # I_bias = V_bias/R_bias = V_ref/(72.5*65535*100499. Ohms)*ADAC
        # I_bias ~= (0.0086 nA/count)*ADAC
        # Small disparity between this and dspid.h?
        if mode == 'diode':
            return 10.e3  # 10 uA
        else:
            k = 1.e3
            rbiasdict = {'standard': 220.1*k,
                         'high_res': 220.1*k,  #DELME #FIXME Is this right?
                         'low_res': 20.1*k}
            rbias = rbiasdict[mode]
            return value*4.096/(72.5*65535*rbias)

    @staticmethod
    def GDAC_to_gain(value):
        """
        Converts from GDAC units to Gain.
        """
        return (2.**16)/value
