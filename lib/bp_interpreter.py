#!/bin/env python

"""
bp_interpreter.py
jlazear
2014-04-14

bp_interpreter object for parsing and repackaging backplane data.

Long description

Example:

<example code here>
"""
version = 20140414
releasestatus = 'beta'


import time
import numpy as np
import redis
import bpconfig_parser
from boards.backplane import Backplane
import comm_buffer as cb


class BPInterpreter(object):
    """
    The bp_interpreter object responsible for parsing and repackaging
    backplane data.
    """
    def __init__(self, bpconfig='boards.bpconfig',
                 host='127.0.0.1', port=6379, password=None,
                 prefix='hke', bpname='testbp', dataname='raw_data',
                 outputname='parsed_data', subchannel='bp_data',
                 pollperiod=.5, maxlen=10000, maxprocess=100):
        self.bpconfig = bpconfig_parser.BPConfig(bpconfig)

        self.redis = redis.Redis(host=host, port=port, password=password)

        self.period = pollperiod
        self.cont = True

        kwargs = {'red': host, 'port': port, 'password': password,
                   'prefix': 'hke', 'bpname': bpname,
                   'maxlen': maxlen, 'max_process': maxprocess,
                   'username': 'bp_interpreter'}

        self.raw_buffer = cb.CommBufferReader(commkey=dataname,
                                              callback='commobj',
                                              **kwargs)
        self.parsed_data = cb.CommBufferWriter(commkey=outputname, **kwargs)
        self.parsed_data_key = self.parsed_data.key
        self.dtype_key = self.parsed_data_key + '_header'
        self.dt = None

        self.subchannel = subchannel

        self.backplane = Backplane(self.bpconfig.boards, self._on_notify)

        # Data storage attribute
        self.data = None

    def start(self):
        """
        Every self.period seconds, parse the raw packets pushed into redis by
        port_owner. Once self.backplane determines that a full frame has been
        parsed, calls self._on_notify.
        """
        while self.cont:
            try:
                self._process_packets()
                time.sleep(self.period)
            except KeyboardInterrupt:
                self.cont = False

    def quit(self):
        self.cont = False

    def _process_packets(self):
        """
        Grab up to maxprocess new packets from port_owner and pipe them off
        to self.backplane to be parsed.
        """
        packets = self.raw_buffer.update()

        for packet in packets:
            raw_packet = packet.payload['data']

            # will trigger _on_notify on new frame
            self.backplane.handlepacket(raw_packet)


    def _on_notify(self):
        """
        This is called every time self.backplane determines it has received a
        new full frame of packets. When called, collects the current data from
        self.backplane, transforms it according to the specifications in the
        bpconfig file, compresses it to a numpy binary file, and pipes it off
        to its new destination.
        """
        self.data = self.backplane.getdata()
        self._apply_transform()
        self._construct_binary_frame()
        self._write_data()
        #HERE ??? Anything left to do?

    def _apply_transform(self):
        """
        Applies the transformations described by the bpconfig file to the
        current data set to construct the data products.
        """
        if self.data is None:
            msg = 'Attempted to binary-ify empty data set.'
            raise BPInterpreterError(msg)

        self.data = self.bpconfig(self.data)

    def _construct_binary_frame(self, force=False):
        """
        Constructs the dtype for the data structured array.

        All binary representations are LITTLE ENDIAN, represented by '<' in
        the dtype.
        """
        if (self.dt is not None) and (force == False):
            return
        fmtdict = {'addr': '<u1',
                   'type': '<a1',
                   'frame': '<u1',
                   'tmux': '<a1',
                   'status': '<u1',
                   'unused': '<a1',
                   'ubc': '<u4',
                   'pic': '<u4',
                   'raw_demod': '<u4',
                   'raw_adac': '<u2',
                   'raw_gdac': '<u2',
                   'nsum': '<u1',
                   'mode': '<a8'}

        names = self.data.keys()
        formats = []
        for name in names:
            if ':' in name:
                mid = name.split(':', 2)[1]
                fmtkeys = fmtdict.keys()
                if mid in fmtkeys:
                    midkey = fmtkeys.index(mid)
                    format = fmtdict[fmtkeys[midkey]]
                else:
                    format = '<f4'
            else:
                format = '<f4'
            formats.append(format)

        self.dt = np.dtype({'names': names, 'formats': formats})
        dtstr = str(self.dt)
        self.redis.setnx(self.dtype_key, dtstr)

    def _write_data(self):
        """
        Encodes the data into binary and writes it to redis.
        """
        values = tuple(self.data.values())

        self.nparray = np.array(values, dtype=self.dt)

        towrite = self.nparray.tostring()
        # self._file_write('toyfile.txt', towrite)
        self._write_to_redis(towrite)

    def _file_write(self, fname, towrite):
        """
        Write to a file. Appends.
        """
        with open(fname, 'a') as f:
            f.write(towrite)

    def _write_to_redis(self, towrite):
        """
        Write to the redis ring buffer in self.parsed_data and
        notify listeners.
        """
        self.parsed_data.send(towrite)
        self.redis.publish(self.subchannel, self.parsed_data_key)


class BPInterpreterError(Exception):
    """
    Generic exception for the BPInterpreter.
    """
    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return self.msg