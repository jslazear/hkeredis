#!/bin/env python

"""
bpconfig_parser.py
jlazear
2014-04-14

Interpreter for HKE backplane config files.

Interprets v2 boards.txt files, aka backplane config files. Also constructs
the transformation table.

Example:

fname = 'docs/boards_test.bpconfig'
bpc = BPConfig(fname)

idict = {'brd5:demod:ch1': 1.,
         'brd5:demod:ch0': 2.}
bpc(idict)
"""
version = 20140414
releasestatus = 'beta'

import string
import shlex
from types import StringTypes
from collections import defaultdict
from tempfile import TemporaryFile
from numpy import interp, loadtxt, diff


class BPConfig(object):
    """
    Parses bpconfig files.

    Constructs a `boards` dictionary describing the physical hardware, and a
    `products` dictionary describing the data products.

    New derived keywords may be added by creating a method in the derived
    methods section, and then adding it to the _derived_dict dictionary with
    the desired keyword label.

    Derived methods should return a 2-element tuple: (trans_func, [*sources]).
    trans_func is a Python function or unbound method that will accept ONLY
    the UNKNOWN arguments to the transformation function, i.e. those that must
    be read from the data frame. Any parameters to the function (e.g. m or b
    in a linear transformation) should be built-in by the derived method call.
    The [*sources] item is a list of the source names. This must be returned
    from the derived method because it cannot know a priori which of the
    arguments are names and which are parameters.

    An instance of this class has a __call__ method defined, so the instance
    may be called with a dictionary of input values to apply the
    transformations.
    """
    def __init__(self, fname):
        self.fname = fname

        # Hardware keywords dictionary
        self._hardware_dict = {'backplane': self._backplane,
                               'board': self._board,
                               'register': self._register,
                               'adr': self._adr,
                               'channel': self._channel}

        # Derived keywords dictionary
        self._derived_dict = {'rename': self._rename,
                              'assign': self._assign,
                              'equal': self._equal,
                              'sum': self._sum,
                              'diff': self._diff,
                              'times': self._times,
                              'div': self._div,
                              'linear': self._linear,
                              'interpolate': self._interpolate}

        # Convenience lists of keys
        self._hardware_keywords = self._hardware_dict.keys()
        self._derived_keywords = self._derived_dict.keys()

        # Hardware state variables
        self.boards = {'boards': {}}
        self._cur_backplane = self.boards['boards']
        self._cur_board = None
        self._cur_register = None

        # Derived state variables
        self.products = DerivedTable()

        # General state variables
        self._cur_line_num = 0
        self._cur_line = None

        # Blacklists
        self._backplane_blacklist = ('boards',)
        self._boards_blacklist = ('reigsters', 'adr')
        self._register_blacklist = ('channels',)
        self._adr_blacklist = ('TofR',)
        self._channel_blacklist = ()

        # Parse boards and products
        self.parse_boards(self.fname)
        self.parse_products(self.fname)
        self.construct_adr_products()

    def parse_boards(self, fname):
        """
        Constructs a boards dictionary describing the physical configuration.
        """
        with open(fname) as f:
            self._current_line_num = -1
            for line in f:
                # Update tracking variables for position in file
                self._cur_line_num += 1
                self._cur_line = line

                # Handle line
                line = self.condition_line(line)
                tokens = self.tokenize(line)
                if tokens:
                    tok0 = tokens[0].lower()
                    if tok0 in self._hardware_keywords:
                        self._handle_hardware(tokens)

    def parse_products(self, fname):
        """
        Constructs a products dictionary describing the derived values.
        """
        # Handle all derived values specified in the file
        with open(fname) as f:
            self._current_line_num = -1
            for line in f:
                # Update tracking variables for position in file
                self._cur_line_num += 1
                self._cur_line = line

                # Handle line
                line = self.condition_line(line)
                tokens = self.tokenize(line)
                if tokens:
                    tok0 = tokens[0].lower()
                    if tok0 not in self._hardware_keywords:
                        self._handle_derived(tokens)

    def construct_adr_products(self):
        """
        Constructs the convenience derived values for boards that have
        specified the `adr` hardware keyword.
        """
        for addr, bdict in self.boards['boards'].items():
            self.construct_adr_product(bdict)

    def construct_adr_product(self, bdict):
        """
        Constructs the convenience derived values for a single board
        specified by its bdict.
        """
        try:
            adrdict = bdict['adr']
            addr = bdict['addr']
            alias = adrdict['alias']
            calfname = adrdict['calfname']

            # Construct <alias>.A and .mA
            t_A = '{0}.A'.format(alias)
            coilI = 'brd{0:02X}:coilI:ch0'.format(addr)
            tfunc_A, sources_A = self._rename(coilI)
            self.products.append(t_A, tfunc_A, sources_A)

            t_mA = '{0}.mA'.format(alias)
            tfunc_mA, sources_mA = self._times(1000., t_A)
            self.products.append(t_mA, tfunc_mA, sources_mA)

            # Interpolate temperatures to construct <alias>.K and .mK
            t_K = '{0}.K'.format(alias)
            demod = 'brd{0:02X}:demod:ch0'.format(addr)
            tfunc_K, sources_K = self._interpolate(demod, calfname)
            self.products.append(t_K, tfunc_K, sources_K)

            t_mK = '{0}.mK'.format(alias)
            tfunc_mK, sources_mK = self._times(1000., t_K)
            self.products.append(t_mK, tfunc_mK, sources_mK)
        except KeyError:
            return



    @staticmethod
    def condition_line(line):
        """
        Conditions a config file line into standard form.
        """
        # Translation table: ,() all go to space.
        tt = string.maketrans(',()', '   ')

        newline = line.translate(tt)
        return newline

    @staticmethod
    def tokenize(line):
        """
        Tokenize a line.
        """
        tokens = shlex.split(line, comments='#')
        return tokens

    @staticmethod
    def parse_arglist(arglist):
        args = []
        kwargs = {}
        for tok in arglist:
            if '=' in tok:
                key, value = tok.split('=', 1)
                kwargs[key] = value
            else:
                args.append(tok)
        return args, kwargs

    def check_target(self, target):
        invalids = tuple((c for c in '0123456789.'))
        if target.startswith(invalids):
            args = (target, self._cur_line_num, self._cur_line)
            raise BPConfigInvalidDerivedName(*args)
        return target

    @staticmethod
    def reverse_bpdict(bpdict):
        reversed_dict = defaultdict(list)
        for addr, bdict in bpdict['boards'].items():
            bname = bdict['name']
            for rtype, rdict in bdict['registers'].items():
                for chaddress, chdict in rdict['channels'].items():
                    chname = chdict['name']
                    chnamedict = {'brd_name': bname, 'brd_addr': addr,
                                  'reg_type': rtype, 'ch_addr': chaddress}

                    # Add name form entries
                    reversed_dict[chname].append(chnamedict)

                    # Add standard form entries
                    chstr = 'ch{0}'.format(chaddress)
                    reversed_dict[chstr].append(chnamedict)
        return dict(reversed_dict)

    @classmethod
    def parse_source(cls, source, boards):
        """
        Converts the name of a source into standard form.

        E.g., 'brd5:demod:"My Thermometer"' -> 'brd05:demod:ch1'
        """
        invalids = tuple((c for c in '0123456789.'))
        if source.startswith(invalids):
            try:
                source = float(source)
            except ValueError:
                raise BPConfigInvalidSourceName(source)
        else:
            colons = source.count(':')
            if colons > 0:
                if colons == 1:
                    try:
                        brd, reg = source.split(':')
                        ch = 'ch0'
                    except ValueError:
                        raise BPConfigInvalidSourceName(source)
                elif colons == 2:
                    try:
                        brd, reg, ch = source.split(':')
                    except ValueError:
                        raise BPConfigInvalidSourceName(source)
#            if ':' in source:
                # try:
                #     brd, reg, ch = source.split(':')
                # except ValueError:
                #     raise BPConfigInvalidSourceName(source)

                ch = ch.strip("'\"")
                reg = reg.strip("'\"")
                brd = brd.strip("'\"")

                rev_dict = cls.reverse_bpdict(boards)

                if not ch:
                    raise BPConfigInvalidSourceName(source)
                try:
                    valids = rev_dict[ch]
                except KeyError:
                    raise BPConfigInvalidSourceName(source)

                if reg:
                    valids = [d for d in valids if d['reg_type'] == reg]

                if brd.startswith('brd'):
                    try:
                        brd_addr = int(brd[3:], 16)
                    except ValueError:
                        raise BPConfigInvalidSourceName(source)
                    valids = [d for d in valids if d['brd_addr'] == brd_addr]
                elif brd:
                    valids = [d for d in valids if d['brd_name'] == brd]

                if len(valids) != 1:
                    raise BPConfigInvalidSourceName(source)

                chdict = valids[0]
                brd_addr = chdict['brd_addr']
                reg_type = chdict['reg_type']
                ch_addr = chdict['ch_addr']

                source = "brd{0:02X}:{1}:ch{2}".format(brd_addr,
                                                       reg_type,
                                                       ch_addr)
        return source


    #---HARDWARE METHODS---#
    def _handle_hardware(self, tokens):
        cmd = tokens[0].lower()
        arglist = tokens[1:]
        args, kwargs = self.parse_arglist(arglist)
        self._hardware_dict[cmd](*args, **kwargs)

    def _backplane(self, **kwargs):
        for key, value in kwargs.items():
            key = key.lower()
            if key in self._backplane_blacklist:
                args = (key, self._cur_line_num, self._cur_line)
                raise BPConfigInvalidHardwareArgKeyword(*args)
            self.boards[key] = value

    def _board(self, addr, type, name, **kwargs):
        try:
            addr = int(addr, 16)
        except (TypeError, ValueError):
            args = ('addr', addr, self._cur_line_num, self._cur_line)
            raise BPConfigInvalidHardwareArgument(*args)

        btype = type.lower()

        if name.startswith('brd'):
            args = ('name', name, self._cur_line_num, self._cur_line)
            raise BPConfigInvalidHardwareArgument(*args)

        bdict = {'registers': {}}

        bdict['addr'] = addr
        bdict['type'] = btype
        bdict['name'] = name

        for key, value in kwargs.items():
            key = key.lower()
            if key in self._boards_blacklist:
                args = (key, self._cur_line_num, self._cur_line)
                raise BPConfigInvalidHardwareArgKeyword(*args)
            bdict[key] = value

        self._cur_board = bdict
        self._cur_backplane[addr] = bdict

    def _adr(self, alias, calfname, **kwargs):
        bdict = self._cur_board
        adrdict = {}
        adrdict['alias'] = alias
        adrdict['calfname'] = calfname

        # Make R(T) interpolation function
        cal = self._cal_load(calfname)
        calTs, calRs = cal.T

        # Reverse arrays if xs are not monotonically increasing
        if not (all(diff(calTs) > 0)):
            calTs = calTs[::-1]
            calRs = calRs[::-1]
        # If, after reverses, still not monotonic, then not valid
        if not (all(diff(calTs) > 0)):
            errstr = "Invalid interpolation file: {0}".format(calfname)
            raise ValueError(errstr)

        RofT = lambda T: interp(T, calTs, calRs)
        adrdict['RofT'] = RofT

        # Add any other keyword arguments to the dictionary
        for key, value in kwargs.items():
            key = key.lower()
            if key in self._adr_blacklist:
                args = (key, self._cur_line_num, self._cur_line)
                raise BPConfigInvalidHardwareArgKeyword(*args)
            adrdict[key] = value

        bdict['adr'] = adrdict

    def _register(self, type, **kwargs):
        rtype = type.lower()

        self._cur_register = {}
        rdict = {'channels': self._cur_register}

        rdict['type'] = rtype

        for key, value in kwargs.items():
            key = key.lower()
            if key in self._register_blacklist:
                args = (key, self._cur_line_num, self._cur_line)
                raise BPConfigInvalidHardwareArgKeyword(*args)
            rdict[key] = value

        try:
            self._cur_board['registers'][rtype] = rdict
        except TypeError:
            args = ('register', self._cur_line_num, self._cur_line)
            raise BPConfigInvalidHardwareCommand(*args)

    def _channel(self, addr, name, **kwargs):
        try:
            addr = int(addr)
        except (TypeError, ValueError):
            args = ('addr', addr, self._cur_line_num, self._cur_line)
            raise BPConfigInvalidHardwareArgument(*args)

        if name.startswith('ch'):
            args = ('name', name, self._cur_line_num, self._cur_line)
            raise BPConfigInvalidHardwareArgument(*args)

        chdict = {}

        chdict['addr'] = addr
        chdict['name'] = name

        for key, value in kwargs.items():
            key = key.lower()
            if key in self._channel_blacklist:
                args = (key, self._cur_line_num, self._cur_line)
                raise BPConfigInvalidHardwareArgKeyword(*args)
            chdict[key] = value

        try:
            self._cur_register[addr] = chdict
        except TypeError:
            args = ('channel', self._cur_line_num, self._cur_line)
            raise BPConfigInvalidHardwareCommand(*args)

    #---DERIVED METHODS---#
    def _handle_derived(self, tokens):
        tokens = [tok for tok in tokens if (tok != '=')]  # Discard '=' token.
        target = self.check_target(tokens[0])
        if len(tokens) == 2:
            source = self.parse_source(tokens[1], self.boards)
            tfunc, sources = self._derived_dict['equal'](source)
            self.products.append(target, tfunc, sources)
        else:
            cmd = tokens[1].lower()
            arglist = tokens[2:]
            args, kwargs = self.parse_arglist(arglist)
            args = [self.parse_source(arg, self.boards) for arg in args]
            kwargs = {key: self.parse_source(value, self.boards)
                      for key, value in kwargs.items()}
            try:
                tfunc, sources = self._derived_dict[cmd](*args, **kwargs)
                self.products.append(target, tfunc, sources)
            except KeyError:
                args = (cmd, self._cur_line_num, self._cur_line)
                raise BPConfigInvalidDerivedCommand(*args)

    @staticmethod
    def _rename(source):
        tfunc = lambda x: x
        sources = [source]
        return (tfunc, sources)

    @staticmethod
    def _assign(source):
        tfunc = lambda: source
        sources = []
        return (tfunc, sources)

    def _equal(self, source):
        if isinstance(source, StringTypes):
            return self._rename(source)
        else:
            return self._assign(source)

    @staticmethod
    def _sum(source1, source2):
        s1str = isinstance(source1, StringTypes)
        s2str = isinstance(source2, StringTypes)
        if s1str and s2str:
            tfunc = lambda x, y: x + y
            sources = [source1, source2]
        elif s1str:
            tfunc = lambda x: x + source2
            sources = [source1]
        elif s2str:
            tfunc = lambda x: x + source1
            sources = [source2]
        else:
            tfunc = lambda: source1 + source2
            sources = []
        return (tfunc, sources)

    @staticmethod
    def _diff(source1, source2):
        s1str = isinstance(source1, StringTypes)
        s2str = isinstance(source2, StringTypes)
        if s1str and s2str:
            tfunc = lambda x, y: x - y
            sources = [source1, source2]
        elif s1str:
            tfunc = lambda x: x - source2
            sources = [source1]
        elif s2str:
            tfunc = lambda x: source1 - x
            sources = [source2]
        else:
            tfunc = lambda: source1 - source2
            sources = []
        return (tfunc, sources)

    @staticmethod
    def _times(source1, source2):
        s1str = isinstance(source1, StringTypes)
        s2str = isinstance(source2, StringTypes)
        if s1str and s2str:
            tfunc = lambda x, y: x*y
            sources = [source1, source2]
        elif s1str:
            tfunc = lambda x: x*source2
            sources = [source1]
        elif s2str:
            tfunc = lambda x: source1*x
            sources = [source2]
        else:
            tfunc = lambda: source1*source2
            sources = []
        return (tfunc, sources)

    @staticmethod
    def _div(source1, source2):
        s1str = isinstance(source1, StringTypes)
        s2str = isinstance(source2, StringTypes)
        if s1str and s2str:
            tfunc = lambda x, y: x/y
            sources = [source1, source2]
        elif s1str:
            tfunc = lambda x: x/source2
            sources = [source1]
        elif s2str:
            tfunc = lambda x: source1/x
            sources = [source2]
        else:
            tfunc = lambda: source1/source2
            sources = []
        return (tfunc, sources)

    @staticmethod
    def _linear(x, m, b):
        xstr = isinstance(x, StringTypes)
        mstr = isinstance(m, StringTypes)
        bstr = isinstance(b, StringTypes)
        if xstr and mstr and bstr:
            tfunc = lambda xx, mm, bb: mm*xx + bb
            sources = [x, m, b]
        elif xstr and mstr:
            tfunc = lambda xx, mm: mm*xx + b
            sources = [x, m]
        elif xstr and bstr:
            tfunc = lambda xx, bb: m*xx + b
            sources = [x, b]
        elif mstr and bstr:
            tfunc = lambda mm, bb: mm*x + bb
            sources = [m, b]
        elif xstr:
            tfunc = lambda xx: m*xx + b
            sources = [x]
        elif mstr:
            tfunc = lambda mm: mm*x + b
            sources = [m]
        elif bstr:
            tfunc = lambda bb: m*x + bb
            sources = [b]
        else:
            tfunc = lambda: m*x + b
            sources = []
        return (tfunc, sources)

    def _interpolate(self, x, calfile):
        cal = self._cal_load(calfile)
        calys, calxs = cal.T

        # Reverse arrays if xs are not monotonically increasing
        if not (all(diff(calxs) > 0)):
            calxs = calxs[::-1]
            calys = calys[::-1]
        # If, after reverses, still not monotonic, then not valid
        if not (all(diff(calxs) > 0)):
            errstr = "Invalid interpolation file: {0}".format(calfile)
            raise ValueError(errstr)

        if isinstance(x, StringTypes):
            tfunc = lambda xx: interp(xx, calxs, calys)
            sources = [x]
        else:
            tfunc = lambda: interp(x, calxs, calys)
            sources = []

        return (tfunc, sources)


    #---UTILITY METHODS---#
    @staticmethod
    def _cal_load(calfname):
        """
        Load a cal curve. This method is more robust against format
        inconsistencies in the cal file, at the cost of a small amount
        of memory.
        """
        try:
            cal = loadtxt(calfname)
            return cal
        except ValueError:
            tmp = TemporaryFile(mode='w+')
            with open(calfname) as f:
                for line in f:
                    if 'UNITS' in line:
                        line = '# ' + line
                    tmp.write(line)
            tmp.seek(0)
            cal = loadtxt(tmp)
            return cal

    def __call__(self, input_dict):
        return self.products(input_dict)


class DerivedTable(object):
    """
    A table of recipes for computing derived values.

    An input dictionary of names and values is used to construct the derived
    values. The table transformations need to be computed only once and can
    be repeatedly applied to a new data set as required.

    The order of the transformations in the table matters, as subsequent
    transformations may depend on the results from previous ones.

    The table is implemented as a Python list. Each element in the list
    describes a transformation. The elements are stored as:

        [target_name, transform_func, (*sources)]

    target_name is the name of the to-be-created variable.

    The transform_func should have any parameters already built-in. It should
    only accept arguments that are sources, i.e.

        def transform_func(source1, source2, ..., sourceN): ...

    transform_func should be a Python function or unbound method.

    (*sources) is a list of source NAMES, i.e. the names that will be used
    by an instance of this class to look up the needed values. It has the form
    (*sources) = (source1, source2, ..., sourceN). It is passed into the
    transform_func as

        transform_func(*(*sources)) == transform_func(source1, source2, ...)

    DerivedTable defines a __call__ method, so when an instance of this class
    is called with a dictionary argument, it uses that dictionary as the
    lookup dictionary to find the inputs to the transformation tables, and
    then outputs a dictionary with the newly derived values added to the
    input dictionary. E.g.,

        dt = DerivedTable()
        dt.append('times_two', lambda x: 2*x, ['original'])
        dt({'original': 5})  # {'original': 5, 'times_two': 10}
    """
    def __init__(self):
        self.transform_list = []

    def append(self, target, transform_func=None, sources=None):
        """
        Adds an item to the transform table.

        If target is not a string, assumes that the transformation is in tuple
        form already and will try to add it directly to the transformation
        list. Otherwise, constructs a transformation tuple and then appends it
        to the list.

        If target is not a transformation tuple, then all of the arguments
        are required.
        """
        if not isinstance(target, StringTypes):
            # Very basic screening of arguments
            try:
                name, tfunc, sources = target
                assert callable(tfunc) == True
                assert isinstance(sources, StringTypes) == False
                for source in sources:
                    assert isinstance(source, StringTypes) == True
            except (ValueError, TypeError, AssertionError):
                raise ValueError("Invalid transformation tuple.")

            self.transform_list.append(target)
        else:
            # Very basic screening of arguments
            try:
                assert callable(transform_func) == True
                assert isinstance(sources, StringTypes) == False
                for source in sources:
                    assert isinstance(source, StringTypes) == True
            except (ValueError, TypeError, AssertionError):
                raise ValueError("Invalid transformation tuple.")

            trans_tuple = (target, transform_func, sources)
            self.transform_list.append(trans_tuple)

    @staticmethod
    def _transform(trans_tuple, input_dict):
        """
        Performs the transformation encoded in `trans_tuple` using data from
        `input_dict`.
        """
        _, trans_func, sources = trans_tuple
        source_values = (input_dict[source] for source in sources)
        value = trans_func(*source_values)
        return value

    def __call__(self, input_dict):
        for trans_tuple in self.transform_list:
            name = trans_tuple[0]
            value = self._transform(trans_tuple, input_dict)
            input_dict[name] = value
        return input_dict

    def __add__(self, toadd):
        for item in toadd:
            self.append(item)
        return self

    def __iter__(self):
        return self.transform_list.__iter__()

    def __repr__(self):
        return "<DerivedTable object: {0}".format(repr(self.transform_list))

    def __str__(self):
        return "DerivedTable object: {0}".format(str(self.transform_list))


#=======================#
#===EXCEPTION CLASSES===#
#=======================#

class BPConfigError(Exception):
    """
    Base class for BPConfig Exceptions.
    """
    pass


class BPConfigHardwareError(BPConfigError):
    """
    Base class for errors on the Hardware Parsing.
    """
    pass


class BPConfigInvalidHardwareArgKeyword(BPConfigHardwareError):
    """
    The user gave a hardware command with an invalid argument keyword.
    """
    def __init__(self, key, linenum, line):
        self.key = key
        self.linenum = linenum
        self.line = line

        self.str = ("Invalid keyword argument key '{0}' on line {1}:\n"
                    "L{1}: {2}").format(self.key, self.linenum, self.line)

    def __str__(self):
        return self.str


class BPConfigInvalidHardwareArgument(BPConfigHardwareError):
    """
    The user gave a hardware command with an invalid argument.
    """
    def __init__(self, key, arg, linenum, line):
        self.key = key
        self.arg = arg
        self.linenum = linenum
        self.line = line

        self.str = ("Invalid argument: '{0}={1}' on line {2}:\n"
                    "L{2}: {3}").format(self.key, self.arg,
                                         self.linenum, self.line)

    def __str__(self):
        return self.str


class BPConfigInvalidHardwareCommand(BPConfigHardwareError):
    """
    The user gave an invalid hardware command.
    """
    def __init__(self, cmd, linenum, line):
        self.cmd = cmd
        self.linenum = linenum
        self.line = line

        self.str = ("Invalid command: '{0}' on line {1}:\n"
                    "L{1}: {2}\nA 'board' command must precede a 'register' "
                    "command, and a 'register' command must precede a "
                    "'channel' command"
                    ).format(self.cmd, self.linenum, self.line)

    def __str__(self):
        return self.str


class BPConfigDerivedError(BPConfigError):
    """
    Base class for errors on the Derived Parsing.
    """
    pass


class BPConfigInvalidDerivedName(BPConfigDerivedError):
    """
    The user tried to assign to an invalid name for a derived value.
    """
    def __init__(self, name, linenum, line):
        self.name = name
        self.linenum = linenum
        self.line = line

        self.str = ("Invalid derived name '{0}' on line {1}:\n"
                    "L{1}: {2}").format(self.name, self.linenum, self.line)

    def __str__(self):
        return self.str


class BPConfigInvalidDerivedCommand(BPConfigDerivedError):
    """
    The user tried to give an invalid derived command.
    """
    def __init__(self, cmd, linenum, line):
        self.cmd = cmd
        self.linenum = linenum
        self.line = line

        self.str = ("Invalid command: '{0}' on line {1}:\n"
                    "L{1}: {2}").format(self.cmd, self.linenum, self.line)

    def __str__(self):
        return self.str


class BPConfigInvalidDerivedArgument(BPConfigDerivedError):
    """
    The user gave a derived command with an invalid argument.
    """
    def __init__(self, key, arg, linenum, line):
        self.key = key
        self.arg = arg
        self.linenum = linenum
        self.line = line

        self.str = ("Invalid argument: '{0}={1}' on line {2}:\n"
                    "L{2}: {3}").format(self.key, self.arg,
                                         self.linenum, self.line)

    def __str__(self):
        return self.str


class BPConfigInvalidSourceName(BPConfigError):
    """
    The user tried to use an invalid source name.
    """
    def __init__(self, name, linenum=None, line=None):
        self.name = name
        self.linenum = linenum
        self.line = line

        if self.linenum and self.line:
            self.str = ("Invalid name: '{0}' on line {1}:\n"
                        "L{1}: {2}").format(self.name, self.linenum,
                                            self.line)
        else:
            self.str = "Invalid name: '{0}'".format(self.name)

    def __str__(self):
        return self.str


