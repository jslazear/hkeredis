#!/bin/env python

"""
comm_buffer.py
jlazear
2014-01-21

Library for interacting with redis receive buffers

Example:

# Must start a redis server instance before running this code
r = redis.Redis()
writer = CommBufferReader(r, commkey='test')
reader = CommBufferWriter(r, commkey='test',
                          max_process=3, callback='commobj')
# writer.empty()    # if already initialized
# reader.reset()    # if already initialized
writer.send(*[CommObjBase() for i in range(5)])
print reader.update()
print reader.update()
print reader.update()
"""
version = 20140121
releasestatus = 'beta'

import redis
import json
import uuid
import time
import datetime
from hashlib import md5
from collections import Iterable
from types import StringTypes

class CommObjBase(object):
    """
    Base Communications Object class.

    Subclassed into type-specific classes.

    The first argument, `first`, is either the JSON-encoded string to be
    converted or the buffer index. All string types will be assumed to be
    JSON-encoded strings to be decoded. All other types will be converted to
    an integer and used as the buffer index.

    The argument `checksum` indicates whether or not a checksum should be
    included (if constructing a packet), or checked (if decoding a packet).
    The default behavior includes checksums in packets and checks checksums
    from packets, if available, and otherwise ignores the check. If decoding
    a packet, a check may be forced by setting `checksum` to True, in which case
    an error is raised if no checksum is present or the checksum does not match.
    If decoding a packet, the checksum may be forced to be ignored by setting
    `checksum` to False.
    """
    def __init__(self, first=-1,
                 sourcename=None,
                 destinationids='all', destinationnames=None,
                 packettype='generic',
                 payload={},
                 checksum = None):
        if isinstance(first, StringTypes):
            self.checksum_flag = checksum
            self.decode(first)
        elif isinstance(first, CommObjBase):
            self.checksum_flag = checksum
            self.decode(str(first))
        else:
            self.bufferindex = int(first)

            self.sourceid = self._get_uuid()
            if sourcename is None:
                self.sourcename = self.sourceid
            else:
                self.sourcename = sourcename

            self.destinationids = self._listify(destinationids)
            if destinationnames is None:
                self.destinationnames = self.destinationids
            else:
                self.destinationnames = self._listify(destinationnames)

            self.timestamp = time.time()
            self.t = datetime.datetime.fromtimestamp(self.timestamp)
            self.hr_timestamp = self.t.isoformat()

            self.packettype = str(packettype)
            # self.payload = {key: str(value) for key, value in payload.items()}
            self.payload = payload

            self.checksum_flag = (True if (checksum is None)
                                       else bool(checksum))

            self._construct_dict()

    def decode(self, jsonstr):
        """
        Parse and store the fields of the specified jsonstr in the CommObj.
        """
        self.encoded = jsonstr
        self.blob, self.checksum = self._extract_checksum(self.encoded)
        if self.checksum_flag is None:
            self.checksum_flag = False if (self.checksum is None) else True

        if self.checksum_flag:
            self._check()

        self.packet_dict = json.loads(self.blob)
        pd = self.packet_dict

        self.bufferindex = pd['buffer_index']

        self.sourceid = pd['source_id']
        self.sourcename = pd['source_name']

        self.destinationids = pd['destination_id']
        self.destinationnames = pd['destination_name']

        self.timestamp = pd['timestamp']
        self.t = datetime.datetime.fromtimestamp(self.timestamp)
        self.hr_timestamp = pd['hr_timestamp']

        self.packettype = pd['packet_type']
        self.payload = pd['packet_payload']

    def _decode(self):
        """
        Returns a JSON-decoded version of the encoded string.

        Mainly used as a check, since one can just get directly at the
        unencoded dictionary in the packet_dict attribute. But these two
        should match, if c is an instance of CommObj:

        >>> c.packet_dict == c.decode()

        up to unicode vs non-unicode string differences.
        """
        return json.loads(self.blob)

    def _check(self, blob=None):
        if blob is None:
            blob = self.blob
        newchecksum = md5(blob).digest()
        if newchecksum != self.checksum:
            self.passed_checksum = False
            raise ChecksumError
        else:
            self.passed_checksum = True

    @classmethod
    def _listify(cls, obj):
        """
        Convert an object to a non-string iterable.

        I.e., turn into a list."""
        if not isinstance(obj, Iterable) or isinstance(obj, StringTypes):
            obj = [obj]

        return obj

    @classmethod
    def _extract_checksum(cls, encoded):
        """
        Separates out the checksum from the JSON blob and returns both. If no
        checksum is present, returns None for the checksum.
        """
        if encoded.endswith(']'):
            blob = encoded[:-18]
            checksum = encoded[-17:-1]
        else:
            blob = encoded
            checksum = None
        return blob, checksum

    def _get_uuid(self, static=True):
        uid = str(uuid.uuid1())
        if static:
            uid = uid[9:]
        return uid

    def _construct_dict(self):
        d = {'buffer_index': self.bufferindex,
             'source_id': self.sourceid,
             'source_name': self.sourcename,
             'destination_id': self.destinationids,
             'destination_name': self.destinationnames,
             'timestamp': self.timestamp,
             'hr_timestamp': self.hr_timestamp,
             'packet_type': self.packettype,
             'packet_payload': self.payload
             }

        self.packet_dict = d
        self.blob = json.dumps(d, sort_keys=True)
        if self.checksum_flag:
          self.checksum = md5(self.blob).digest()
          checkstr = '[{0}]'.format(self.checksum)
        else:
          checkstr = ''

        self.encoded = self.blob + checkstr
        self.passed_checksum = True

    def __str__(self):
        return self.encoded

    def __unicode__(self):
        return unicode(self.encoded)

    def __call__(self):
        return self.encoded

    def __repr__(self):
        old = super(CommObjBase, self).__repr__()
        old = old[:-2]
        new = ": type {0}, from {1}, to {2}, at {3}>".format(self.packettype,
                                                      self.sourcename,
                                                      self.destinationnames,
                                                      self.hr_timestamp)
        return old + new

    def __eq__(self, other):
        return str.__eq__(self.encoded, str(other))

    def __getitem__(self, key):
        return self.packet_dict[key]

    def items(self):
        return self.packet_dict.items()


class LogObj(CommObjBase):
    """
    A log-type Comm Object.
    """
    def __init__(self, orig, logargs=[],
                 sourcename=None,
                 destinationids='all', destinationnames=None):
        if isinstance(orig, StringTypes):
            orig = CommObjBase(orig, checksum=False)

        log_type = orig.packettype
        log_args = logargs
        orig_packet = orig.blob

        payload = {'type': log_type,
                   'arguments': log_args,
                   'original': orig_packet}

        self.log_type = log_type
        self.arguments = log_args
        self.original = orig_packet

        super(LogObj, self).__init__(first=-1,
                 sourcename=sourcename,
                 destinationids=destinationids,
                 destinationnames=destinationnames,
                 packettype='log',
                 payload=payload,
                 checksum = None)


class DataObj(CommObjBase):
    """
    A data-type Comm Object.
    """
    def __init__(self, data, data_type='raw',
                 sourcename=None,
                 destinationids='all', destinationnames=None):
        payload = {'type': data_type, 'data': data}
        self.data_type = data_type
        self.data = data

        super(DataObj, self).__init__(first=-1,
                 sourcename=sourcename,
                 destinationids=destinationids,
                 destinationnames=destinationnames,
                 packettype='data',
                 payload=payload,
                 checksum = None)


class CommandObj(CommObjBase):
    """
    A command-type Comm Object.
    """
    def __init__(self, command, arguments=[],
                 sourcename=None,
                 destinationids='all', destinationnames=None):
        payload = {'command': command,
                   'arguments': arguments}

        self.command = command
        self.arguments = arguments

        super(CommandObj, self).__init__(first=-1,
                 sourcename=sourcename,
                 destinationids=destinationids,
                 destinationnames=destinationnames,
                 packettype='command',
                 payload=payload,
                 checksum = None)


class CommBufferReader(object):
    """
    An object for listening on a communications buffer link.

    :Arguments:
        `commkey` - <str> the redis key serving as the communications channel.
        `indexkey` - <str> the redis key storing the current index of the
                     communications channel. If None, then is `commkey`_index.
        `max_process` - <int> the maximum number of packets that may be
                        read in a single update step
        `index_init` - <int> the value to initialize the local index to. 0
                       indicates that all stored data should be retrieved.
                       -1 indicates that the listener should be initialized
                       as current, and will only retrieve new packets.
        `callback` - <str/func> the callback function to apply to each item in
                     the returned list. May be either a string identifier of
                     the function desired, or may pass in your own function.
                     May also use None or False to indicate no callback should
                     be used. Defaults to CommObj.

    Call the update() method to retrieve a list of new packets in the
    communications channel.

    Callback Functions: If passing in your own callback function, then the
    callback function must be a one-argument function that accepts a string
    and returns whatever object you would like to transform it into.

    This object is not threaded and contains no flow control. It is expected
    to be controlled by an external flow control mechanism. This object is
    not thread safe and should not be shared between threads unless external
    locks are implemented.
    """

    lua = """
          local datalost = 0
          local server_index = tonumber(redis.call('GET', KEYS[2]))
          local local_index = tonumber(ARGV[1])

          if server_index == local_index then
              return {datalost, local_index}
          end

          local len = tonumber(redis.call('LLEN', KEYS[1]))
          local min_index = server_index - len
          if min_index > local_index then
              datalost = min_index - local_index
              local_index = min_index
          end

          local bottom = local_index - min_index
          local top = math.min(bottom + ARGV[2] - 1, server_index - min_index - 1)
          local data = redis.call('LRANGE', KEYS[1], bottom, top)
          data[#data + 1] = datalost
          data[#data + 1] = top + min_index + 1
          return data
          """

    def __init__(self, red='127.0.0.1', port=6379, password=None,
                 bpname='', prefix='',
                 commkey='commkey', indexkey=None,
                 max_process=100, index_init=0,
                 username=None, registeruser=True,
                 callback=None, verbose=True,
                 **kwargs):
        if isinstance(red, (redis.Redis, redis.StrictRedis)):
            self.redis_conn = red
            self.pool = None
        elif isinstance(red, redis.ConnectionPool):
            self.pool = red
            self.redis_conn = redis.Redis(connection_pool=self.pool)
        elif isinstance(red, StringTypes):
            self.pool = redis.ConnectionPool(host=red, port=port,
                                             password=password)
            self.redis_conn = redis.Redis(connection_pool=self.pool)

        self.userid = str(uuid.uuid1())[9:]
        if username is None:
            username = self.userid
        self.username = username

        self.max_process = max_process

        self.prefix = prefix
        self.bpname = bpname
        baselist = [str(i) for i in (self.prefix, self.bpname) if i]
        self._basestr = '.'.join(baselist + ['{0}'])
        # basestr = self.prefix + self.name + "{0}"
        globallist = [str(i) for i in (self.prefix,) if i]
        self._globalstr = '.'.join(globallist + ['{0}'])

        self.key = self._basestr.format(commkey)
        # self.key = prefix + name + commkey
        self.indexkey = ((self.key + '_index') if (indexkey is None)
                         else indexkey)
        self.index = index_init

        self.usernameskey = self._basestr.format('usernames')
        self.useridskey = self._basestr.format('userids')
        self.globalusernameskey = self._globalstr.format('usernames')
        self.globaluseridskey = self._globalstr.format('userids')

        self.num_packets_lost = 0

        self._update = self.redis_conn.register_script(self.lua)

        self.set_callback(callback)

        self.verbose = verbose

        self._initialize_comm()
        if registeruser:
            self._register_self()

    def _initialize_comm(self):
        """
        Initialize the comm channel, if un-initialized.

        Initializes only the server-side index variable, not the length
        variable, because the reader does not care about the length. The
        first writer must initialize the length.
        """
        self.redis_conn.setnx(self.indexkey, 0)

    def _register_self(self):
        """Register username and userid with redis."""
        self.redis_conn.hset(self.usernameskey, self.username, self.userid)
        self.redis_conn.hset(self.useridskey, self.userid, self.username)
        self.redis_conn.hset(self.globalusernameskey, self.username,
                             self.userid)
        self.redis_conn.hset(self.globaluseridskey, self.userid,
                             self.username)

    def update(self, verbose=None):
        """
        Returns a list of the new packets in the channel since the last
        update, and advances the internal index.

        Will not return more than max_process packets.
        """
        if verbose is None:
            verbose = self.verbose

        keys = [self.key, self.indexkey]
        args = [self.index, self.max_process]
        response = self._update(keys=keys, args=args)
        self.index = int(response.pop())
        datalost = int(response.pop())
        self.num_packets_lost += datalost
        if datalost and verbose:
            errstr = "WARNING: Some data was unrecoverable! {0} packets lost."
            print errstr.format(datalost)
        if self._cbflag:
            response = [self._cb(item) for item in response]
        return response

    def reset(self, index_init=0, cb=None):
        """
        Resets the state of the listener.
        """
        self.index = index_init
        self.num_packets_lost = 0
        if cb is not None:
            self.set_callback(cb)

    def _empty(self):
        """
        Empties and re-initializes the comm link. For debugging only!
        """
        pipe = self.redis_conn.pipeline()
        pipe.delete(self.key)
        pipe.set(self.indexkey, 0)
        pipe.execute()
        self.reset()

    def set_callback(self, callback):
        """
        Sets the callback function to be applied to packets.

        The callback function must be a one-argument function that accepts a
        string and returns whatever object you would like to transform it
        into.
        """
        if (callback is None) or (callback is False):
            self._cbflag = False
            self._cb = None
        elif isinstance(callback, StringTypes):
            self._cbflag = True
            cbdict = {'commobj': lambda x: CommObjBase(x)}
            self._cb = cbdict[callback]


class CommBufferWriter(object):
    """
    An object for talking to a communications buffer link.
    """
    def __init__(self, red='127.0.0.1', port=6379, password=None,
                 bpname='', prefix='',
                 username=None, registeruser=True,
                 commkey='commkey', indexkey=None, lengthkey=None,
                 maxlen=10000, **kwargs):
        if isinstance(red, (redis.Redis, redis.StrictRedis)):
            self.redis_conn = red
            self.pool = None
        elif isinstance(red, redis.ConnectionPool):
            self.pool = red
            self.redis_conn = redis.Redis(connection_pool=self.pool)
        elif isinstance(red, StringTypes):
            self.pool = redis.ConnectionPool(host=red, port=port,
                                             password=password)
            self.redis_conn = redis.Redis(connection_pool=self.pool)

        self.userid = str(uuid.uuid1())[9:]
        if username is None:
            username = self.userid
        self.username = username

        self.prefix = prefix
        self.bpname = bpname
        baselist = [str(i) for i in (self.prefix, self.bpname) if i]
        self._basestr = '.'.join(baselist + ['{0}'])
        # basestr = self.prefix + self.name + "{0}"
        globallist = [str(i) for i in (self.prefix,) if i]
        self._globalstr = '.'.join(globallist + ['{0}'])

        self.key = self._basestr.format(commkey)
        self.indexkey = ((self.key + '_index') if (indexkey is None)
                         else indexkey)
        self.lengthkey = ((self.key + '_length') if (lengthkey is None)
                         else lengthkey)

        self.usernameskey = self._basestr.format('usernames')
        self.useridskey = self._basestr.format('userids')
        self.globalusernameskey = self._globalstr.format('usernames')
        self.globaluseridskey = self._globalstr.format('userids')

        self._initialize_comm(maxlen)

        self.maxlength = int(self.redis_conn.get(self.lengthkey))
        if registeruser:
            self._register_self()

    def _register_self(self):
        """Register username and userid with redis."""
        self.redis_conn.hset(self.usernameskey, self.username, self.userid)
        self.redis_conn.hset(self.useridskey, self.userid, self.username)
        self.redis_conn.hset(self.globalusernameskey, self.username,
                             self.userid)
        self.redis_conn.hset(self.globaluseridskey, self.userid,
                             self.username)

    def _initialize_comm(self, maxlen=10000):
        """
        Initialize the comm channel, if un-initialized.

        Initializes only the server-side index variable, not the length
        variable, because the reader does not care about the length. The
        first writer must initialize the length.
        """
        self.redis_conn.setnx(self.indexkey, 0)
        self.redis_conn.setnx(self.lengthkey, maxlen)

        self.maxlength = int(self.redis_conn.get(self.lengthkey))

    def send(self, *values):
        """
        Append an object to the communications buffer and update the index.

        This command is atomic.
        """
        # Must send strings, so make sure everything is a string
        values = [str(val) for val in values]
        pipe = self.redis_conn.pipeline()
        num = len(values)
        pipe.rpush(self.key, *values)
        pipe.ltrim(self.key, -self.maxlength, -1)
        pipe.incr(self.indexkey, num)
        pipe.execute()

    def _empty(self):
        """
        Empty the communications buffer.
        """
        pipe = self.redis_conn.pipeline()
        pipe.delete(self.key)
        pipe.set(self.indexkey, 0)
        pipe.execute()


#====== ERROR CLASSES ======#

class ChecksumError(Exception):
    """An error indicating a checksum failed."""
    def __init__(self, obj):
        self.obj = obj

    def __str__(self):
        return "Packet {obj} failed checksum.".format(obj=self.obj)