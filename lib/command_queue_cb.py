import redis
import time
import serial

import comm_buffer as cb


class CommandQueue(object):
    """
    A redis command queue.

    Make a new method and add it to _cmddict to use it.
    """
    def __init__(self, host='127.0.0.1', port=6379, password=None,
                 prefix='hke', bpname='testbp',
                 username=None, registeruser=True,
                 global_queue=True,
                 queuename='command_requests', responsename='activity_log',
                 errorname='errors', dataname='raw_data',
                 pollperiod=.25, maxlen=10000,
                 serport='/dev/tty.usbserial-DAWYA0PI', baudrate=115200,
                 timeout=0., max_process=1):
        self.buffer = ''

        self.redis = redis.Redis(host=host, port=port, password=password)
        self.ser = serial.Serial(port=serport, baudrate=baudrate,
                                    timeout=timeout)
        self.period = pollperiod
        self.cont = True

        kwargs = {'red': host, 'port': port, 'password': password,
                   'prefix': 'hke', 'bpname': bpname,
                   'maxlen': maxlen, 'max_process': max_process,
                   'username': username}

        self.command_queue = cb.CommBufferReader(commkey=queuename,
                                                 callback='commobj',
                                                 **kwargs)
        self.response_buffer = cb.CommBufferWriter(commkey=responsename,
                                                   **kwargs)
        self.errors_buffer = cb.CommBufferWriter(commkey=errorname, **kwargs)
        self.data_buffer = cb.CommBufferWriter(commkey=dataname, **kwargs)

        self._cmddict = {'p': self.p,  # Print to host terminal
                         's': self.s}  # Send to serial port

    def start(self):
        while self.cont:
            try:
                self._process_commands()
                self._read_serial_port()
                time.sleep(self.period)
            except KeyboardInterrupt:
                self.cont = False
        else:
            self.ser.close()

    def quit(self):
        self.cont = False

    def _process_commands(self):
        cmds = self.command_queue.update()

        if cmds: print cmds  #DELME

        for cmd in cmds:
            print "Received cmd:", cmd()  #DELME
            payload = cmd['packet_payload']
            command = payload['command']
            args = payload['arguments']
            try:
                self._cmddict[command](*args)
                logcmd = cb.LogObj(cmd, logargs=['success'])
                self.response_buffer.send(logcmd)
            except KeyError:
                self.default(cmd, *args)
                logcmd = cb.LogObj(cmd, logargs=['failed'])
                self.response_buffer.send(logcmd)

    # @staticmethod
    # def _extract_command(packet):
    #     payload = packet['packet_payload']
    #     cmd = payload['command']
    #     args = payload['arguments']
    #     cmdlist = [str(cmd)] + [str(arg) for arg in args]

    #     command = ' '.join(cmdlist)
    #     return command

    def _read_serial_port(self):
        while self.ser.inWaiting():
            self.buffer += self.ser.read(self.ser.inWaiting())

        if '\n' in self.buffer:
            textlist = self.buffer.split('*')
            self.buffer = ''
            packetlist = []
            restlist = [textlist[0], ]

            for item in textlist[1:]:
                packet, waste, rest = item.partition('\n')
                if waste == '\n':
                    packetlist.append(packet)
                    restlist.append(rest)
                else:
                    self.buffer += packet

            packetobjs = [cb.DataObj(packet) for packet in packetlist]
            msg = ''.join(restlist)
            msgobj = cb.DataObj(msg, 'message')

            self.data_buffer.send(*packetobjs)
            self.response_buffer.send(msgobj)

        # while self.ser.inWaiting():
        #     readblock = self.buffer + self.ser.read(self.ser.inWaiting())
        #     readlist = readblock.split('\r\n')
        #     self.buffer = readlist.pop()
        #     resplist = [rl for rl in readlist
        #                 if (not rl.startswith('*') and rl != '')]

        #     if resplist and (resplist != ['']):
        #         dataobjs = [cb.DataObj(resp) for resp in resplist]
        #         self.response_buffer.send(*dataobjs)

    def default(self, cmd, *args):
        try:
            argstr = ' '.join([str(arg) for arg in args])
        except ValueError:
            argstr = str(args)
        print "Default command: {0} {1}".format(cmd, argstr)

    def p(self, *args):
        try:
            argstr = ' '.join([str(arg) for arg in args])
        except ValueError:
            argstr = str(args)
        print "Print: {0}".format(argstr)

    def s(self, *args):
        try:
            argstr = ' '.join([str(arg) for arg in args])
        except ValueError:
            argstr = str(args)

        towrite = argstr + '\r\n'

        print "Sending to serial port: {0}".format(repr(towrite)) #DELME
        self.ser.write(towrite)


#DELME Everything after this.
def test():
    kwargs = {'username': 'testuser',
             'serport': '/Users/jlazear/pty1',
              # 'serport': '/dev/tty.usbserial-DAWYA0PI'
             }
    cq = CommandQueue(**kwargs)
    cq.start()

if __name__ == '__main__':
    test()