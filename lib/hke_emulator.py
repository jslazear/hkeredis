#!/bin/env python

"""
hke_emulator.py
jlazear
2014-04-22

Emulator for a backplane.

Reads a bpconfig file and sends out corresponding toy packets every 1s.

Example:

<example code here>
"""
version = 20140422
releasestatus = 'beta'

import time
import serial

import bpconfig_parser


class Emulator(object):
    """
    Emulates an HKE backplane.
    """
    def __init__(self, bpconfig, port, newdspid=True, period=1.):
        self.period = period

        self.packetdict = {'pmaster': '*{0}M{1}05FFFFFFFF0000011C',
            'tread': ('*{0}T{1}@300204B4D00003333002060CB000033330020720A0000'
                      '3333002086250000333300209B14000033330020AFA40000333300'
                      '20C2EB000033330020D856000033330020EDEF000033330021009A'
                      '00003333002113FA00003333002127A10000333300005477000033'
                      '33000675910000333300101AC800003333002044FD0000333342'),
            'dspid': ('*{0}D{1}000011E5BD000C00000011E4DC000C00000011E530000C'
                      '00000011E563000C00000011E54C000C00000011E490000C000000'
                      '11E545000C00000011E517000C000019002000FFEE000D80323A89'
                      '2DC0000D0000000000000000000430EF00000000000000000101'),
            'dspid2': ('*{0}D{1}000011E5BD000C00000011E4DC000C00000011E530000'
                      'C00000011E563000C00000011E54C000C00000011E490000C00000'
                      '011E545000C00000011E517000C000019002000FFEE000D80323A8'
                      '92DC0000D0000000000000000000430EF00000000000000000101C'
                      '6')}

        if newdspid:
            self.packetdict['dspid'] = self.packetdict['dspid2']

        self.times = [(198.)/4000. for i in range(21)]
        self.times[0] = 40./4000.
        self.packets = ['' for i in range(21)]
        self.addresses = range(-1, 20)
        self.addresses[0] = 255

        self.bpconfig = bpconfig_parser.BPConfig(bpconfig)

        boards = self.bpconfig.boards['boards']

        keys = self.packetdict.keys()

        for addr, bdict in boards.items():
            btype = bdict['type']
            bkey = None
            for key in keys:
                if btype.startswith(key):
                    bkey = key
            packet = self.packetdict[bkey]
            index = ((addr + 1) % 256) % 21
            self.packets[index] = packet

        self.cur_frame = 0
        self.last_index = -1

        self.ser = serial.Serial(port, baudrate=115200)
        self.cont = True

    def send_packet(self, index):
        packet = self.packets[index]
        addr = self.addresses[index]
        addrstr = '{0:02X}'.format(addr)
        framestr = '{0:02X}'.format(self.cur_frame)

        if packet:
            towrite = packet.format(addrstr, framestr) + '\r\n'
            self.ser.write(towrite)

    def quit(self):
        self.cont = False

    def start(self):
        self.t0 = time.time()
        self.tnext = self.t0 + self.times[0]
        while self.cont:
            try:
                t = time.time()
                if t >= self.tnext:
                    self.last_index += 1
                    if self.last_index >= 21:
                        self.last_index = 0
                        self.cur_frame += 1
                    self.send_packet(self.last_index)
                    self.tnext += self.times[self.last_index]
                time.sleep(.001)
            except KeyboardInterrupt:
                self.cont = False
        else:
            self.ser.close()


