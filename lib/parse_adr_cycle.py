#!/bin/env python

"""
parse_adr_cycle.py
jlazear
2014-09-24

ADR file parsing library.

A library for parsing ADR files. The primary class is ADRFileParser, whose
parse_from_file() method may be used to generate a parse tree for a full ADR
cycle file.

Requires the PLY library (http://www.dabeaz.com/ply/ OR pip install ply).

Uses a one-pass (lex -> parse) system with a stateful lexer to construct the
full parse tree. The stateful lexer is required because the "expressions"
(e.g. messages, state names, conditions, etc.) can be complicated and are not
easily (not possibly?) reconciled with other lexemes.

Returns a parse tree as a generic node-tree. The tree must be converted to a
sensible state machine by some other codebase.

Also contains supporting classes for lexing and expression parsing, which may
be exercised individually if desired.

The dictionaries and grammars are as follows:

# General lexemes (in modified BNF)

<COMMENT> ::= "\#[^\n]*"     # regex; discarded
<NEWLINE> ::= "(\r)?\n"      # regex; returns to base state; discarded
<WHITESPACE> ::= "( |\t)"    # regex; discarded; has maximally low
                             # precedence.

# ---File parsing grammar (in BNF)---

<States> ::= <State> <States>
           | <State>
           | empty
<State> ::= <STATE> <Lines>
<Lines> ::= <Line> <Lines>
          | <Line>
<Line> ::= <NameLine>
         | <CmdLine>
         | <WaitforLine>
         | <MessageLine>
<NameLine> ::= <NAME> <STRING>
<CmdLine> ::= <CMD> <STRING>
<WaitforLine> ::= <WAITFOR> <Expression>
<MessageLine> ::= <MESSAGE> <STRING>

# File parsing lexemes (in modified BNF)

<STATE> ::= "(state|STATE)"  # regex
<NAME> ::= "NAME:?"          # regex; enters String lexing state
<CMD> ::= "CMD:?"            # regex; enters String lexing state
<WAITFOR> ::= "WAITFOR:?"    # regex; enters Expression state
<MESSAGE> ::= "MESSAGE:?"    # regex; enters String lexing state

# ---String parsing grammar---
# NOTE: string parsing is a mutually exclusive lexer state, but the parser is
        not stateful.

No grammar rules relevant to the string state.

# String lexemes (in modified BNF)

<STRING> ::= "[^\#\n]+"  # in regex form


# ---Expression parsing grammar (in BNF)---
# NOTE: expression parsing is a mutually exclusive lexer state, but the parser
        is not stateful.

<Expression> ::= <CompoundExpr> <GOTO> <IDENTIFIER>
         | <CompoundExpr>
         | <GOTO> <IDENTIFIER>
         | empty
<CompoundExpr> ::= <NOT> <CompoundExpr>
                 | <BROPEN> <CompoundExpr> <BRCLOSE>
                 | <CompoundExpr> <AND> <CompoundExpr>
                 | <CompoundExpr> <OR> <CompoundExpr>
                 | <WaitCondition>
<WaitCondition> ::= <IDENTIFIER> <COMP> <FLOAT>
                  | <FLOAT> <COMP> <IDENTIFIER>
                  | <IDENTIFIER> <COMP> <IDENTIFIER>
                  | <IDENTIFIER>

# Expression parsing lexemes (in modified BNF)

<GOTO> ::= "[Gg][Oo][Tt][Oo]"  # in regex form; case-insensitive "GOTO"
<NOT> ::= "[Nn][Oo][Tt]"       # in regex form; case-insensitive "NOT"
<AND> ::= "[Aa][Nn][Dd]"       # in regex form; case-insensitive "AND"
<OR> ::= "[Oo][Rr]"            # in regex form; case-insensitive "OR"
<BROPEN> ::= "\("              # in regex form
<BRCLOSE> ::= "\)"             # in regex form
<IDENTIFIER> ::= "[a-zA-Z_][._a-zA-Z0-9]*"  # in regex form; lower precedence
<FLOAT> ::= "[-+]?[0-9]+\.?[0-9]*([eE][-+]?[0-9]+)?"  # in regex form
# Note on precedence: OR < AND < NOT


Examples:

# FULL FILE PARSING (Primary usage)
fileparser = ADRFileParser()
parsedfile = fileparser.parse_from_file()
parsedfile.print_tree()

# FILE LEXING
f = open('docs/adr_cycle_pumped3.txt')
ftxt = ''.join(f.readlines())
filelexer = ADRFileLexer()
filelexer.build()
filelexer.lexer.input(ftxt)

fmtstr = "{0:>04} {1:>04} {2:12} {3:<20}"
header = fmtstr.format('LNUM', 'LPOS', 'TYPE', 'VALUE')
print header
print len(header)*'-'
for token in fl.lexer:
    print fmtstr.format(token.lineno, token.lexpos, token.type, token.value)
"""
version = 20140923
releasestatus = 'beta'

import types
import shlex
import re

import ply.lex as lex
import ply.yacc as yacc

import bpconfig_parser


# -----------------
# --- NODE TREE ---
# -----------------

class Node(object):
    """
    The basic building block of the ADR state machine node tree.

    Supports indexing, which indexes the node's children.
    """
    def __init__(self, nodetype, children=None, nodevalue=None):
         self.nodetype = nodetype
         if children:
              self.children = children
         else:
              self.children = []
         self.nodevalue = nodevalue

    def print_tree(self, depth=None):
        if depth is None:
            depth = -1
        depth = depth + 1

        pstr = "{0}-{1} ({2})"
        print pstr.format(' |'*depth, self.nodetype, self.nodevalue)
        for child in self.children:
            if isinstance(child, Node):
                child.print_tree(depth)
            else:
                print "{0}-{1}".format(' |'*depth, child)

    def __str__(self):
        return 'Node({0}, {1}, {2})'.format(self.nodetype, self.nodevalue,
                                            self.children)

    def __repr__(self):
        return '<Node({0}, {1}, {2})>'.format(self.nodetype, self.nodevalue,
                                              self.children)

    # Implement iterator methods. Behaves as a the same container as children.
    def __len__(self):
        return self.children.__len__()

    def __iter__(self):
        return self.children.__iter__()

    def __getitem__(self, key):
        return self.children.__getitem__(key)

    def __setitem__(self, key):
        return self.children.__setitem__(key)

    def __reversed__(self):
        return self.children.__reversed__()

    def __contains__(self):
        return self.children.__contains__()

# -------------
# --- LEXER ---
# -------------

class ADRFileLexer(object):
    # List of token names.   This is always required
    tokens = ['STATE',
              'NEWLINE',
              'FLOAT',
              'COMP',
              'BROPEN',
              'BRCLOSE',
              'IDENTIFIER',
              'STRING',
              'COMMENT'
              ]

    states = (('expression', 'exclusive'),
              ('string', 'exclusive'),
              )

    reserved = {'name': 'NAME',
                'cmd': 'CMD',
                'waitfor': 'WAITFOR',
                'message': 'MESSAGE'}

    expr_reserved = {'or': 'OR',
                     'and': 'AND',
                     'not': 'NOT',
                     'goto': 'GOTO'}

    # Regular expression rules for simple tokens
    t_expression_BROPEN  = r'\('
    t_expression_BRCLOSE  = r'\)'
    t_expression_COMP = r'<|>'

    # A string containing ignored characters (spaces and tabs)
    t_ANY_ignore  = ' \t'


    def __init__(self):
        self.tokens = (self.tokens + self.reserved.values()
                                   + self.expr_reserved.values())

    def t_STATE(self, t):
        r'(state|STATE)(\s)*(:)?'
        t.value = t.value.strip().rstrip(':').strip()
        return t

    def t_keyword(self, t):
        r'[a-zA-Z_][._a-zA-Z0-9]*(\s)*:?'
        t.value = t.value.rstrip(':').strip()
        # Check for reserved words...
        try:
            t.type = self.reserved[t.value.lower()]
        except KeyError:
            self.t_ANY_error(t)
        if t.type == 'WAITFOR':
            t.lexer.begin('expression')
        else:
            t.lexer.begin('string')
        return t

    def t_expression_IDENTIFIER(self, t):
        r'[a-zA-Z_][._a-zA-Z0-9]*'
        # Check for reserved words...
        t.type = self.expr_reserved.get(t.value.lower(), 'IDENTIFIER')
        return t

    def t_expression_FLOAT(self, t):
        r'([-+]?[0-9]+(\.)?[0-9]*([eE][-+]?[0-9]+)?)|([-+]?[0-9]*(\.)?[0-9]+([eE][-+]?[0-9]+)?)'
        t.value = float(t.value)
        return t

    # Define a rule so we can track line numbers
    def t_newline(self, t):
        r'(\r)?\n'
        t.lexer.lineno += 1

    # Newlines in the expression or string states exit to default state
    def t_expression_string_newline(self, t):
        r'(\r)?\n'
        t.lexer.lineno += 1
        t.lexer.begin('INITIAL')

    def t_string_STRING(self, t):
        r'[^\#\n]+'
        t.value = t.value.strip()
        return t

    def t_ANY_COMMENT(self, t):
        r'\#[^\n]*'
        pass  # discard token

    # Error handling rule
    def t_ANY_error(self, t):
        toprint = "Illegal token: value = {0}, linenum = {1}, lexpos = {2}"
        print "---ERROR---"
        print toprint.format(t.value[0], t.lexer.lineno, t.lexer.lexpos)
        print "Skipping character..."
        t.lexer.skip(1)

    def build(self, **kwargs):
        self.lexer = lex.lex(module=self, **kwargs)

# --------------
# --- PARSER ---
# --------------

class ADRFileParser(object):
    """
    Constructs the parse tree of a full ADR cycle file.

    Call the parse_from_file() method of an instance of this class on the
    file. Returns a parse tree of the full file.

    Uses a one-pass (lex -> parse) system with a stateful lexer to construct
    the full parse tree. The stateful lexer is required because the
    "expressions" (e.g. messages, state names, conditions, etc.) can be
    complicated and are not easily (not possibly?) reconciled with other
    lexemes.

    The NEWLINE and COMMENT tokens are deliberately unused.

    Example:
    fileparser = ADRFileParser()
    parsedfile = fileparser.parse_from_file()
    parsedfile.print_tree()
    """
    def __init__(self, bpconfig='boards.bpconfig'):
        self.bpconfig = bpconfig_parser.BPConfig(bpconfig)
        self.lexer = ADRFileLexer()
        self.lexer.build()
        self.tokens = self.lexer.tokens
        # The grammar is small, so cheap to recompute LALR table every time.
        self.precedence = (('left', 'OR'),
                           ('left', 'AND'),
                           ('left', 'NOT'))
        self.parser = yacc.yacc(module=self, write_tables=False, debug=False)

    def print_tokens(self, fname):
        f = open(fname, 'r')
        ftxt = ''.join(f.readlines())
        self.lexer.lexer.input(ftxt)
        for token in self.lexer.lexer:
            print token
        self.lexer.build()

    def parse(self, data):
        if data:
            return self.parser.parse(data, self.lexer.lexer, 0, 0, None)
        else:
            return []

    def parse_from_file(self, f):
        if not isinstance(f, types.StringTypes):
            ftxt = ''.join(f.readlines())
        else:
            with open(f, 'r') as ff:
                ftxt = ''.join(ff.readlines())
        node_tree = self.parse(ftxt)
        self._check_transitions(node_tree)
        return node_tree

    def _check_transitions(self, states_tree):
        """Checks that all transitions are to valid states."""
        # Define a crawler to find all values in all nodes of a certain type
        def node_crawler(node, nodetype):
            values = [node.nodevalue] if (node.nodetype == nodetype) else []
            for child in node:
                values += node_crawler(child, nodetype)
            return values

        states = node_crawler(states_tree, 'name')
        destinations = node_crawler(states_tree, 'destination')

        errorflag = False
        for destination in destinations:
            if destination not in states:
                errorflag = True
                msg = "ERROR: GOTO 'destination' {0} is not a known state!"
                print msg.format(destination)
        if errorflag:
            raise ParseError()

    def _condition_cmd(self, cmd):
        """Modifies cmd string so it may be completed with a .format(**data)"""
        cmdlist = shlex.split(cmd)
        addr = cmdlist[0]
        invalids = tuple((c for c in '0123456789.'))

        # Check if the address is in hex
        try:
            intaddr = int(addr, 16)
        except ValueError:
            intaddr = None

        # If not a valid (0-255) hex address and not a valid name, raise error
        if (not (0 <= intaddr <= 255)) and addr.startswith(invalids):
            raise ParseError('Command has invalid address: {0}'.format(cmd))
        elif not addr.startswith(invalids):
            intaddr = self._lookup_address(addr)
            cmdlist[0] = str(intaddr)

        # Construct look-up dictionary of actions that suffices may trigger.
        # All actions accept board address so they may be made board-sensitive.
        def K(T, addr):
            RofT = self.bpconfig.boards['boards'][addr]['adr']['RofT']
            return RofT(T)
        mK = lambda T, addr: K(T*1.e-3, addr)
        k = lambda x, addr: 1000*x
        M = lambda x, addr: 1.e6*x
        m = lambda x, addr: 1.e-3*x
        kOhm = lambda x, addr: 1000*x
        MOhm = lambda x, addr: 1.e6*x
        mOhm = lambda x, addr: 1.e-3*x
        fdict = {'K': K,          # Kelvin
                 'mK': mK,        # milliKelvin
                 'k': k,          # kilo
                 'M': M,          # mega
                 'm': m,          # milli
                 'kOhm': kOhm,    # kiloOhm
                 'mOhm': mOhm,    # milliOhm
                 'MOhm': MOhm}    # megaOhm

        valids = re.compile(r'(?P<number>[0-9]+(.[0-9]+)?)(?P<suffix>\D+)')
        for i, item in enumerate(cmdlist[1:], start=1):
            match = valids.match(item)
            if match:
                number = float(match.group('number'))
                suffix = match.group('suffix')
                try:
                    item = int(fdict[suffix](number, intaddr))
                    cmdlist[i] = str(item)
                except KeyError:
                    errstr = 'Invalid suffix: "{0}" in "{1}"'
                    raise ParseError(errstr.format(suffix, cmd))

        cmdstr = ' '.join(cmdlist)
        return cmdstr

    def _lookup_address(self, alias):
        """Looks up the board address corresponding to an alias."""
        # Construct the look-up table
        lut = {}
        boards = self.bpconfig.boards['boards']
        for addr, bdict in boards.items():
            try:
                key = bdict['adr']['alias']
                lut[key] = addr
            except KeyError:
                pass

        # Look up value in table
        try:
            address = lut[alias]
        except KeyError:
            raise ParseError('Invalid ADR alias: {0}'.format(alias))
        return address

    # --- PARSING RULES ---
    def p_Start_states(self, p):
        """Start : States"""
        p[0] = p[1]

    def p_Start(self, p):
        """Start : empty"""
        p[0] = Node('states', [], '')

    def p_States_two(self, p):
        """States : State States"""
        p[0] = p[2]
        # Add to beginning of list -- parser reduces last elements first
        p[0].children.insert(0, p[1])

    def p_States_one(self, p):
        """States : State"""
        p[0] = Node('states', [p[1]], 'states')

    def p_State(self, p):
        """State : STATE Lines"""
        p[0] = p[2]

    def p_Lines_two(self, p):
        """Lines : Line Lines"""
        p[0] = p[2]
        # Add to beginning of list -- parser reduces last elements first
        p[0].children.insert(0, p[1])

    def p_Lines(self, p):
        """Lines : Line"""
        p[0] = Node('state', [p[1]], 'state')

    def p_Line(self, p):
        """Line : NameLine
                | CmdLine
                | WaitforLine
                | MessageLine"""
        p[0] = p[1]

    def p_NameLine(self, p):
        """NameLine : NAME STRING"""
        p[0] = Node('name', nodevalue=p[2])

    def p_CmdLine(self, p):
        """CmdLine : CMD STRING"""
        cmd = self._condition_cmd(p[2])
        p[0] = Node('cmd', nodevalue=cmd)

    def p_WaitforLine(self, p):
        """WaitforLine : WAITFOR Expression"""
        p[0] = Node('waitfor', [p[2]], 'waitfor')

    def p_MessageLine(self, p):
        """MessageLine : MESSAGE STRING"""
        p[0] = Node('message', [], p[2])

    def p_Expression_compgoto(self, p):
        """Expression : CompoundExpr GOTO IDENTIFIER"""
        p3 = Node('destination', [], p[3])
        p[0] = Node('expression', [p[1], p3], 'expr_goto')
        # p[0] = [Node('expression', [p[1]], 'expr'), p3]

    def p_Expression_comp(self, p):
        """Expression : CompoundExpr"""
        p[0] = Node('expression', [p[1]], 'expr')

    def p_Expression_goto(self, p):
        """Expression : GOTO IDENTIFIER"""
        p2 = Node('destination', [], p[2])
        p[0] = Node('expression', [p2], 'goto')

    def p_CompoundExpr_not(self, p):
        """CompoundExpr : NOT CompoundExpr"""
        p[0] = Node('not', [p[2]], '!')

    def p_CompoundExpr_paren(self, p):
        """CompoundExpr : BROPEN CompoundExpr BRCLOSE"""
        p[0] = p[2]

    def p_CompoundExpr_and(self, p):
        """CompoundExpr : CompoundExpr AND CompoundExpr"""
        p[0] = Node('and', [p[1], p[3]], 'and')

    def p_CompoundExpr_or(self, p):
        """CompoundExpr : CompoundExpr OR CompoundExpr"""
        p[0] = Node('or', [p[1], p[3]], 'or')

    def p_CompoundExpr_waitcond(self, p):
        """CompoundExpr : WaitCondition"""
        p[0] = p[1]

    def p_WaitCondition_comp1(self, p):
        """WaitCondition : IDENTIFIER COMP FLOAT"""
        p1 = Node('identifier', [], p[1])
        p3 = Node('float', [], p[3])
        p[0] = Node('compare', [p1, p3], p[2])

    def p_WaitCondition_comp2(self, p):
        """WaitCondition : FLOAT COMP IDENTIFIER"""
        p1 = Node('float', [], p[1])
        p3 = Node('identifier', [], p[3])
        p[0] = Node('compare', [p1, p3], p[2])

    def p_WaitCondition_comp3(self, p):
        """WaitCondition : IDENTIFIER COMP IDENTIFIER"""
        p1 = Node('identifier', [], p[1])
        p3 = Node('identifier', [], p[3])
        p[0] = Node('compare', [p1, p3], p[2])

    def p_WaitCondition(self, p):
        """WaitCondition : IDENTIFIER"""
        p[0] = Node('identifier', [], p[1])

    def p_empty(self, p):
        """empty : """
        pass

    def p_error(self, p):
        toprint = 'Parsing error: line = {0}, token_type = {1}, token = {2}'
        print toprint.format(p.lineno, p.type, p.value)


class ParseError(Exception):
    """Error class for the ADR file parser."""
    def __init__(self, msg=''):
        self.msg = msg
    def __str__(self):
        return "Failed to parse ADR cycle. " + self.msg
