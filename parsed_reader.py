#!/bin/env python

"""
parsed_reader.py
jlazear
2014-04-21

An example reader for how to grab and reconstruct parsed data products out of
redis.

Example:

pr = ToyParsedReader()
data = pr.read_data()
"""
version = 20140421
releasestatus = 'beta'

import numpy as np
import redis
import lib.comm_buffer as cb


class ToyParsedReader(object):
    """
    A class describing how to reconstruct parsed data products out of redis.
    """
    def __init__(self, host='127.0.0.1', port=6379, password=None,
                 prefix='hke', bpname='testbp', dataname='parsed_data'):
        kwargs = {'prefix': prefix, 'bpname': bpname}
        self.data_reader = cb.CommBufferReader(commkey=dataname, **kwargs)
        self.header_key = self.data_reader.key + '_header'
        print "header_key = ", repr(self.header_key) #DELME

        r = redis.Redis(host=host, port=port, password=password)

        print "header_key = ", repr(self.header_key) #DELME
        dtstr = r.get(self.header_key)
        print "dtstr = ", repr(dtstr) #DELME
        self.dt = np.dtype(eval(dtstr))

    def read_data(self):
        """
        Read some data. The amount depends on how the data_reader was
        configured. Always reads the NEXT N packets, not the last N.
        """
        datastr = ''.join(self.data_reader.update())
        data = np.frombuffer(datastr, dtype=self.dt)
        return data
