import redis
import time
import serial

class CommandQueue(object):
    """
    A redis command queue.

    Make a new method and add it to _cmddict to use it.
    """
    def __init__(self, host, port=6379, password=None,
                 queue='hke.commandqueue', response='hke.response',
                 pollperiod=.25, maxresponses=10000,
                 serport='/dev/tty.usbserial-DAWYA0PI', baudrate=115200,
                 timeout=0.):
        self.redis = redis.Redis(host=host, port=port, password=password)
        self.ser = serial.Serial(port=serport, baudrate=baudrate,
                                    timeout=timeout)
        self.buffer = ''

        self.cont = True
        self._queuename = queue
        self._responsename = response
        self._responseindex = response + 'index'
        self.period = 1.
        self.maxresponses = maxresponses

        self._next = None

        self._cmddict = {'p': self.p,  # Print to host terminal
                         's': self.s}  # Send to serial port

    def start(self):
        while self.cont:
            try:
                self._get_command()
                self._read_serial_port()
                time.sleep(self.period)
            except KeyboardInterrupt:
                self.cont = False
        else:
            self.ser.close()

    def _get_command(self):
        # next = self.redis.rpop(self._queuename)
        pipe = self.redis.pipeline()
        self.redis.transaction(self._rpoprpush,
                               self._responsename, self._queuename)
        if self._next:
            try:
                cmd, args = self._next.split(' ', 1)
            except ValueError:
                cmd = next.strip()
                args = ''
            args = args.split(' ')
            try:
                self._cmddict[cmd](*args)
            except KeyError:
                self.default(cmd, *args)
            self._next = None

    def _rpoprpush(self, pipe):
        temp = pipe.lrange(self._queuename, -1, -1)
        if temp:
            self._next = temp[0]
            pipe.multi()
            pipe.rpop(self._queuename)
            pipe.rpush(self._responsename, self._next)
            pipe.ltrim(self._responsename, -self.maxresponses, -1)
            pipe.incr(self._responseindex)

    def _read_serial_port(self):
        while self.ser.inWaiting():
            readblock = self.buffer + self.ser.read(self.ser.inWaiting())
            readlist = readblock.split('\r\n')
            self.buffer = readlist.pop()
            resplist = [rl for rl in readlist
                        if (not rl.startswith('*') and rl != '')]

            if resplist and (resplist != ['']):
                print "Sending to hke.response {0}".format(str(resplist)) #DELME
                # self.redis.rpush(self._responsename, *resplist)
                # self.redis.ltrim(self._responsename, -self.maxresponses, -1)
                self._push_and_trim(*resplist)

    def _push_and_trim(self, *values):
        pipe = self.redis.pipeline()
        num = len(values)
        pipe.rpush(self._responsename, *values)
        pipe.ltrim(self._responsename, -self.maxresponses, -1)
        pipe.incr(self._responseindex, num)
        pipe.execute()

    def default(self, cmd, *args):
        try:
            argstr = ' '.join([str(arg) for arg in args])
        except ValueError:
            argstr = str(args)
        print "Default command: {0} {1}".format(cmd, argstr)

    def p(self, *args):
        try:
            argstr = ' '.join([str(arg) for arg in args])
        except ValueError:
            argstr = str(args)
        print "Print: {0}".format(argstr)

    def s(self, *args):
        try:
            argstr = ' '.join([str(arg) for arg in args])
        except ValueError:
            argstr = str(args)

        towrite = argstr + '\n'

        print "Sending to serial port: {0}".format(repr(towrite)) #DELME
        self.ser.write(towrite)


