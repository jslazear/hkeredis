import serial
import threading
import redis
import uuid
import time

class ReadThread(threading.Thread):
    """
    Thread for reading from the serial port and writing data to redis.
    """
    def __init__(self, serial_conn, redis_conn,
                 pubchannel='hke.response_notify', lifetime=600.):
        super(ReadThread, self).__init__()

        self.ser = serial_conn
        self.red = redis.Redis(connection_pool=redis_conn)

        self.pubchannel = pubchannel
        self.lifetime = 600.  # in seconds

        self.buffer = ''
        self.cont = False

    def run(self):
        self.cont = True

        while self.cont:
            self._read_serial_port()
            time.sleep(0.1)

    def _read_serial_port(self):
        while self.ser.inWaiting():
            readblock = self.buffer + self.ser.read(self.ser.inWaiting())
            readlist = readblock.split('\r\n')
            self.buffer = readlist.pop()
            resplist = [rl for rl in readlist
                        if (not rl.startswith('*') and rl != '')]

            self._pushpub_list(resplist)

    def _pushpub_list(self, publist):
        if not (publist and (publist != [''])):
            return

        for item in publist:
            h = 'hke.' + str(uuid.uuid1())
            # self._push(item, h)
            # self._publish(h)
            self._publish(item) #DELME


    def _push(self, item, h=None, lifetime=None):
        if h is None:
            h = 'hke.' + str(uuid.uuid1())
        if lifetime is None:
            lifetime = self.lifetime
        print "Setting to {0} for {2} seconds: {1}".format(h, item,
                                                           lifetime)  #DELME
        self.red.setex(h, item, lifetime)

    def _publish(self, key, channel=None):
        if channel is None:
            channel = self.pubchannel

        print "Publishing to {0}: {1}".format(channel, key)  #DELME
        # msg = "New message in [{0}]".format(key)
        # self.red.publish(channel, msg)
        self.red.publish(channel, key) #DELME

    def quit(self):
        self.cont = False
        try:
            del self.ser
        except AttributeError:
            pass


class WriteThread(threading.Thread):
    """
    Thread for writing to the serial port and reading commands from redis.
    """
    def __init__(self, serial_conn, redis_conn, subchannel='hke.cmd_notify'):
        super(WriteThread, self).__init__()

        self.ser = serial_conn
        self.red = redis.Redis(connection_pool=redis_conn)
        self.pubsub = self.red.pubsub()
        self.pubsub.subscribe(subchannel)

        self.subchannel = subchannel

        self.cont = False

    def run(self):
        self.cont = True

        for msg in self.pubsub.listen():
            print "msg = ", repr(msg) #DELME
            if not self.cont:
                self.pubsub.unsubscribe(self.subchannel)
            cmd = msg['data']
            try:
                if cmd.startswith('quit'):
                    self.pubsub.unsubscribe(self.subchannel)
            except AttributeError:
                pass

            if msg['type'] == 'message':
                self._process_cmd(cmd)

    def _process_cmd(self, cmd):
        cmd = str(cmd) + "\r\n"
        print "Sending command to backplane: {0}".format(repr(cmd))
        self.ser.write(cmd)

    def quit(self):
        self.cont = False
        self.pubsub.unsubscribe(self.subchannel)
        try:
            del self.ser
        except AttributeError:
            pass


def initialize(serport='/dev/tty.usbserial-DAWYA0PI', baudrate=115200,
               redhost='xuzear.mooo.com', redport=6379,
               pubchannel='hke.response_notify', subchannel='hke.cmd_notify'):
    ser = serial.Serial(port=serport, baudrate=baudrate)
    redconnpool = redis.ConnectionPool(host=redhost, port=redport)

    rt = ReadThread(ser, redconnpool, pubchannel=pubchannel)
    wt = WriteThread(ser, redconnpool, subchannel=subchannel)

    return ser, redconnpool, rt, wt

def stop(ser, rt, wt):
    rt.quit()
    wt.quit()
    ser.close()

if __name__ == '__main__':
    ser, redconnpool, rt, wt = initialize()

    # rt.start()
    # wt.start()
