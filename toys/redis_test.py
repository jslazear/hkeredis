import redis
import time
import threading

rlock = threading.RLock()

class RedisComm(object):
    """
    A simple redis communication class.
    """
    def __init__(self, host, port=6379, password=None,
                 readkey='piper', writekey='piper'):
        self.redis = redis.Redis(host=host, port=port, password=password)
        self.readkey = readkey
        self.readvalue = None
        self.writekey = writekey
        self.writevalue = None

        self.redisthread = RedisThread(host, port, password,
                                       readkey, writekey,
                                       self)

        self.redisthread.start()

    def change_read(self, message):
        self.readvalue = message
        print message

    def write(self, message):
        with rlock:
            self.redisthread.messages.append(message)

    def quit(self):
        self.redisthread.quit()

    def read(self):
        print self.redisthread.read()


class RedisThread(threading.Thread):
    def __init__(self, host, port, password, readkey, writekey, userthread):
        super(RedisThread, self).__init__()
        self.userthread = userthread
        self.messages = []
        self.cont = True

        self.readkey = readkey
        self.writekey = writekey

        self.redis = redis.Redis(host=host, port=port, password=password)
        self.readvalue = self.redis.getset(readkey, 'initread')
        self.redis.getset(writekey, 'initwrite')

    def run(self):
        while self.cont:
            newreadvalue = self.read()
            if newreadvalue != self.readvalue:
                self.report(newreadvalue)
                self.readvalue = newreadvalue

            if self.messages:
                with rlock:
                    towrite = self.messages.pop(0)
                self.write(towrite)

            time.sleep(.25)

    def read(self, key=None):
        if key is None:
            key = self.readkey
        return self.redis.get(key)

    def write(self, key, value=None):
        if value is None:
            value = key
            key = self.writekey
        return self.redis.set(key, value)

    def report(self, message):
        """Report to user thread a message."""
        self.userthread.change_read(message)

    def quit(self):
        self.cont = False